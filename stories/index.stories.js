import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from '../app/components/Button';
import BigButton from '../app/components/BigButton';
import Link from '../app/components/Link';
import Input from '../app/components/Input';
import Label from '../app/components/Label';
import ActionGroup from '../app/components/ActionGroup';
import PageWrapper from '../app/components/PageWrapper';
import Section from '../app/components/Section';
import Logo from '../app/components/Logo';
import Header from '../app/components/Header';
import Paragraph from '../app/components/Paragraph';
import PageHeader from '../app/components/PageHeader';
import Footer from '../app/components/Footer';
import Icon from '../app/components/Icon';
import Recipient from '../app/components/Recipient';
import Order from '../app/components/Order';
import Product from '../app/components/Product';
import Menu from '../app/components/Menu';
import UserInfo from '../app/components/UserInfo';
import ActivityIndicator from '../app/components/ActivityIndicator';
import ProductDetails from '../app/components/ProductDetails';

storiesOf('Activity Indicator', module).add('standard', () => (
  <ActivityIndicator loading />
));

storiesOf('Menu', module).add('standard', () => <Menu />);

storiesOf('Icon', module)
  .add('standard', () => (
    <div>
      <Icon onClick={action('clicked')} icon="truck">
        Hello Button
      </Icon>
      <Icon onClick={action('clicked')} icon="truck" color>
        Hello Button
      </Icon>
    </div>
  ))
  .add('avec label', () => (
    <div>
      <Icon onClick={action('clicked')} icon="truck" label="Livraison">
        Hello Button
      </Icon>
      <Icon onClick={action('clicked')} icon="truck" label="Livraison" color>
        Hello Button
      </Icon>
    </div>
  ));

storiesOf('Button', module).add('with text', () => (
  <Button onClick={action('clicked')}>Hello Button</Button>
));

storiesOf('BigButton', module).add('with text', () => (
  <Section>
    <BigButton
      onClick={action('clicked')}
      headline="Nouvelle livraison"
      text="Partout dans le monde"
      icon="truck"
    />
    <BigButton
      onClick={action('clicked')}
      headline="Mes livraisons"
      icon="history"
    />
    <BigButton
      onClick={action('clicked')}
      headline="Mes destinataires"
      icon="users"
    />
    <BigButton onClick={action('clicked')} headline="Mon comptes" icon="user" />
    <BigButton
      onClick={action('clicked')}
      headline="Envoyer à un ami"
      icon="plane"
    />
  </Section>
));

storiesOf('Link', module).add('with text', () => (
  <Link onClick={action('clicked')} href="http://www.perdu.com">
    Créer un compte
  </Link>
));

storiesOf('Input', module)
  .add('default', () => <Input onClick={action('clicked')} />)
  .add('with placeholder', () => (
    <Input
      onClick={action('clicked')}
      placeholder="Nom d'utilisateur ou email"
    />
  ))
  .add('with value', () => (
    <Input onClick={action('clicked')} value="John Doe" />
  ))
  .add('with label', () => (
    <Label label="Nom d'utilisateur ou email">
      <Input onClick={action('clicked')} />
    </Label>
  ))
  .add('with label and placeholder', () => (
    <Label label="Nom d'utilisateur ou email">
      <Input
        onClick={action('clicked')}
        placeholder="Nom d'utilisateur ou email"
      />
    </Label>
  ))
  .add('with label and value', () => (
    <Label label="Nom d'utilisateur ou email">
      <Input onClick={action('clicked')} value="John Doe" />
    </Label>
  ));

storiesOf('Recipients List', module).add('standard', () => (
  <PageWrapper>
    <Recipient
      name="Jonathan Sigg"
      address="Rue du caro 32"
      postalCode="1630"
      locality="Bulle"
      phone="0797180703"
    />
    <Recipient
      name="Jonathan Sigg"
      address="Rue du caro 32"
      postalCode="1630"
      locality="Bulle"
      phone="0797180703"
    />
    <Recipient
      name="Jonathan Sigg"
      address="Rue du caro 32"
      postalCode="1630"
      locality="Bulle"
      phone="0797180703"
    />
  </PageWrapper>
));

storiesOf('Orders List', module).add('standard', () => (
  <PageWrapper>
    <Order
      creationDateTime="10.04.2019"
      recipient={{ name: 'John Doe' }}
      items={[{ name: 'Grand bouquet' }]}
    />
    <Order
      creationDateTime="10.04.2019"
      recipient={{ name: 'John Doe' }}
      items={[{ name: 'Petit bouquet' }]}
    />
  </PageWrapper>
));

storiesOf('Bouquets List', module).add('standard', () => (
  <PageWrapper>
    <Product id="1" name="S" price={35} donation={10} />
    <Product id="2" name="M" price={65} donation={20} />
    <Product id="3" name="L" price={95} donation={30} />
  </PageWrapper>
));

storiesOf('Bouquet Details', module).add('standard', () => (
  <PageWrapper>
    <ProductDetails id="1" name="Grand bouquet" price={35} donation={10} />
  </PageWrapper>
));

storiesOf('User info', module).add('standard', () => (
  <UserInfo
    name="Jonathan Sigg"
    email="jonathan@techniconcept.ch"
    address="Ancien-comté 33"
    postalCode="1635"
    locality="La Tour"
    phone="026 123 45 56"
    mobile="079 123 45 56"
  />
));

storiesOf('Login Page', module).add('default', () => (
  <PageWrapper>
    <PageHeader>
      <Logo compact />
    </PageHeader>
    <Label label="Nom d'utilisateur">
      <Input
        onClick={action('clicked')}
        name="username"
        placeholder="Nom d'utilisateur ou email"
      />
    </Label>
    <Label label="Mot de passe">
      <Input onClick={action('clicked')} name="password" type="password" />
    </Label>
    <Button type="submit" onClick={action('clicked')}>
      Connexion
    </Button>
    <ActionGroup>
      <Link onClick={action('clicked')} href="http://www.perdu.com">
        Créer un compte
      </Link>
      <Link onClick={action('clicked')} href="http://www.perdu.com">
        Mot de passe oublié
      </Link>
    </ActionGroup>

    <Header center>XXL Fondation</Header>
    <Paragraph center teaser>
      Aidez-nous à lutter contre la malnutrition et la famine en offrant des
      fleurs ; le bénéfice va entièrement à cette cause.
    </Paragraph>
  </PageWrapper>
));

storiesOf('Home Page', module).add('default', () => (
  <PageWrapper>
    <PageHeader>
      <div>
        <Header center>XXL Fondation</Header>
        <Paragraph center teaser>
          Aidez-nous à lutter contre la malnutrition et la famine en offrant des
          fleurs ; le bénéfice va entièrement à cette cause.
        </Paragraph>
      </div>
    </PageHeader>

    <Section>
      <BigButton
        onClick={action('clicked')}
        headline="Nouvelle livraison"
        text="Partout dans le monde"
        icon="truck"
      />
      <BigButton
        onClick={action('clicked')}
        headline="Mes livraisons"
        icon="history"
      />
      <BigButton
        onClick={action('clicked')}
        headline="Mes destinataires"
        icon="users"
      />
      <BigButton
        onClick={action('clicked')}
        headline="Mon compte"
        icon="user"
      />
      <BigButton
        onClick={action('clicked')}
        headline="Envoyer à un ami"
        icon="plane"
      />
    </Section>

    <Footer />
  </PageWrapper>
));
