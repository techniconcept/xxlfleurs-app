module.exports = function(grunt) {
  const appConfig = {
    git: {
      projectUrl: 'https://bitbucket.org/techniconcept/xxlfleurs-app',
    },
    slack: {
      channel: '#xxlfleurs_dev',
    },
  };

  // Permet d'afficher le temps d'exécution de chaque tâche, suite à un build
  require('time-grunt')(grunt);

  grunt.loadNpmTasks('grunt-slack-notifier');
  grunt.loadNpmTasks('grunt-gitinfo');
  grunt.loadNpmTasks('grunt-exec');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    secret: grunt.file.readJSON('secret.json'),

    appConfig,

    gitinfo: {},

    sshconfig: {
      dev: {
        host: '<%= secret.deployDev.host %>',
        username: '<%= secret.deployDev.username %>',
        // Authentification via la clé SSH
        agent: process.env.SSH_AUTH_SOCK,
      },
      prod: {
        host: '<%= secret.deployProd.host %>',
        username: '<%= secret.deployProd.username %>',
        // Authentification via la clé SSH
        agent: process.env.SSH_AUTH_SOCK,
      },
    },

    exec: {
      deployDev: {
        command: [
          // build app
          'npm run build:preprod',
          // sync build folder with server
          'rsync -rv build/ <%= secret.deployDev.username %>@<%= secret.deployDev.host %>:<%= secret.deployDev.site_dir %>',
        ].join(' && '),
        options: {
          config: 'dev',
        },
      },
      deployProd: {
        command: [
          // build app
          'npm run build',
          // sync build folder with server
          'rsync -rv build/ <%= secret.deployProd.username %>@<%= secret.deployProd.host %>:<%= secret.deployProd.site_dir %>',
        ].join(' && '),
        options: {
          config: 'prod',
        },
      },
    },

    slack_notifier: {
      deployProd: {
        options: {
          token: '<%= secret.slack.token %>',
          channel: '<%= appConfig.slack.channel %>',
          text:
            'New version deployed on production server!\r\n' +
            'Last commit: <%= appConfig.git.projectUrl %>/commits/<%= gitinfo.local.branch.current.SHA %>\r\n' +
            'Url: <%= secret.deployProd.url %>',
          as_user: '<%= secret.slack.username %>',
          parse: 'full',
          link_names: true,
          unfurl_links: true,
          unfurl_media: true,
        },
      },
      deployDev: {
        options: {
          token: '<%= secret.slack.token %>',
          channel: '<%= appConfig.slack.channel %>',
          text:
            'New version deployed on development server!\r\n' +
            'Last commit: <%= appConfig.git.projectUrl %>/commits/<%= gitinfo.local.branch.current.SHA %>\r\n' +
            'Url: <%= secret.deployDev.url %>',
          as_user: '<%= secret.slack.username %>',
          parse: 'full',
          link_names: true,
          unfurl_links: true,
          unfurl_media: true,
        },
      },
    },
  });

  // Autoloader pour les tâches
  require('load-grunt-tasks')(grunt);

  grunt.registerTask('default', ['menu']);

  grunt.registerTask('deployDev', [
    'exec:deployDev',
    'gitinfo',
    'slack_notifier:deployDev',
  ]);

  grunt.registerTask('deployProd', [
    'exec:deployProd',
    'gitinfo',
    'slack_notifier:deployProd',
  ]);
};
