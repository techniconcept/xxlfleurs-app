# XXL Fleurs - Application web

## Développement

* Make sure that you have Node.js v8.10 and npm v5 or above installed.
* Clone this repo using 
```
git clone git@bitbucket.org:techniconcept/xxlfleurs-app.git
```
* Move to the appropriate directory:
```
cd xxlfleurs-app
```
* Run
```
npm run start
```
and open `http://localhost:3000`

### Structure
* app : source de l'application
    * components : contient tous les composants de l'application
    * containers : point d'entrée des différentes page de l'application
    * images
    * queries : les requêtes utilisés pour graphql
    * tests
    * utils
* build : L'application qui sera déployée
* internals : 
    * webpack : dossier de configuration dee build (dev/prod)
* public : 
    * dossier contenant les éléments à copier dans le dossier build lors du build :)
* stories : 
    * contenu du storybook

## Storybook
```
npm run storybook
```

## Déploiement
### Env. de développement
```
grunt deployDev
```
### Env. de production
```
grunt deployProd
```

## URL
### Env. local
* backend : http://local.xxlfleurs-backend.ch
* app : http://localhost:3000

### Env. de développement
* backend : https://dev.fleurs-backend.techniconcept.ch
* app : https://dev.fleurs.techniconcept.ch

### Env. de production
* backend : https://fleurs-backend.xxlfondation.ch
* app : https://fleurs.xxlfondation.ch

## Code du backend
* https://bitbucket.org/techniconcept/xxlfleurs-backend
