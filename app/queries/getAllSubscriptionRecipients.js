import gql from 'graphql-tag';

const GET_ALL_SUBSCRIPTION_RECIPIENTS = gql`
  query GetAllSubscriptionsRecipients($id: ID!) {
    subscriptions {
      get(id: $id) {
        subscriptionActive
        subscriptionDate
        subscriptionRecipients {
          id
          name
          postalCode
          locality
          address
          phone
          country
          event
          eventText
          shippingDate
        }
      }
    }
    profile {
      card
      current {
        id
        name
        address
        postalCode
        locality
        country
        phone
        mobile
        email
      }
    }
    message {
      all {
        id
        title
        showField
      }
    }
  }
`;

export default GET_ALL_SUBSCRIPTION_RECIPIENTS;
