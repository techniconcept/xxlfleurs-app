import gql from 'graphql-tag';

const SET_SUBSCRIPTION_ACTIVE = gql`
  mutation setSubscriptionActive($subscription: ID!, $token: String) {
    profile {
      addCard(token: $token) {
        id
      }
    }
    subscription {
      pay(subscription: $subscription) {
        id
        subscriptionActive
        subscriptionDate
      }
    }
  }
`;

export default SET_SUBSCRIPTION_ACTIVE;
