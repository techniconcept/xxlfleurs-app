import gql from 'graphql-tag';

const GET_SUBSCRIPTION_ACTIVE = gql`
  query GetSubscriptionActive {
    profile {
      subscriptionActive
    }
  }
`;

export default GET_SUBSCRIPTION_ACTIVE;
