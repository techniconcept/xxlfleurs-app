import gql from 'graphql-tag';

const GET_ALL_MESSAGES = gql`
  query GetAllMessages {
    message {
      all {
        id
        title
        showField
      }
    }
  }
`;

export default GET_ALL_MESSAGES;
