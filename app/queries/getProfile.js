import gql from 'graphql-tag';

const GET_PROFILE = gql`
  query GetProfile {
    profile {
      current {
        id
        name
        address
        postalCode
        locality
        country
        phone
        mobile
        email
      }
    }
  }
`;

export default GET_PROFILE;
