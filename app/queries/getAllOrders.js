import gql from 'graphql-tag';

const GET_ALL_ORDERS = gql`
  query GetAllOrders {
    order {
      all {
        id
        shippingDate
        recipient {
          id
          name
          status
        }
        items {
          id
          name
        }
      }
    }
  }
`;

export default GET_ALL_ORDERS;
