import gql from 'graphql-tag';

const CREATE_ORDER = gql`
  mutation createOrder($item: ID!, $recipient: ID) {
    order {
      create(item: $item, recipient: $recipient) {
        id
        sku
        items {
          id
          name
          price
          donation
        }
        recipient {
          id
          name
          address
          locality
          postalCode
          phone
          country
        }
      }
    }
  }
`;

export default CREATE_ORDER;
