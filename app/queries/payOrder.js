import gql from 'graphql-tag';

const PAY_ORDER = gql`
  mutation payOrder($order: ID!, $token: String) {
    profile {
      addCard(token: $token) {
        id
      }
    }
    order {
      pay(order: $order) {
        id
      }
    }
  }
`;

export default PAY_ORDER;
