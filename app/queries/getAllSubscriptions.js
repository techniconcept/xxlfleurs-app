import gql from 'graphql-tag';

const GET_ALL_SUBSCRIPTIONS = gql`
  query GetAllSubscriptions {
    subscriptions {
      all {
        id
        creationDate
      }
    }
  }
`;

export default GET_ALL_SUBSCRIPTIONS;
