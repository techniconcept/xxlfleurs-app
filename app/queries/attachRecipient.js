import gql from 'graphql-tag';

const ATTACH_RECIPIENT = gql`
  mutation createOrder($order: ID!, $recipient: ID!) {
    order {
      attachRecipient(order: $order, recipient: $recipient) {
        id
        sku
        items {
          id
          name
        }
        recipient {
          id
          name
          postalCode
          locality
          address
          phone
        }
      }
    }
  }
`;

export default ATTACH_RECIPIENT;
