import gql from 'graphql-tag';

const UPDATE_SUBSCRIPTION_RECIPIENT = gql`
  mutation updateSubscriptionRecipient(
    $recipient: ID!
    $name: String!
    $address: String!
    $postalCode: String!
    $locality: String!
    $country: String!
    $phone: String!
    $email: String
    $event: String
    $eventText: String
    $shippingDate: DateTime
  ) {
    subscriptionRecipient {
      update(
        recipient: $recipient
        input: {
          name: $name
          address: $address
          postalCode: $postalCode
          locality: $locality
          country: $country
          phone: $phone
          email: $email
          event: $event
          eventText: $eventText
          shippingDate: $shippingDate
        }
      ) {
        id
        name
        address
        postalCode
        locality
        country
        phone
        email
        event
        eventText
        shippingDate
      }
    }
  }
`;

export default UPDATE_SUBSCRIPTION_RECIPIENT;
