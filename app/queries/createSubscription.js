import gql from 'graphql-tag';

const CREATE_SUBSCRIPTION = gql`
  mutation createSubscription {
    subscription {
      create {
        id
      }
    }
  }
`;

export default CREATE_SUBSCRIPTION;
