import gql from 'graphql-tag';

const UPDATE_PROFILE = gql`
  mutation UpdateProfileAndPassword(
    $name: String!
    $address: String
    $postalCode: String
    $locality: String
    $country: String
    $phone: String!
    $mobile: String
    $email: String
  ) {
    profile {
      update(
        input: {
          name: $name
          address: $address
          postalCode: $postalCode
          locality: $locality
          country: $country
          phone: $phone
          mobile: $mobile
          email: $email
        }
      ) {
        id
        name
        address
        postalCode
        locality
        country
        phone
        mobile
        email
      }
    }
  }
`;

export default UPDATE_PROFILE;
