import gql from 'graphql-tag';

const GET_SUBSCRIPTION_RECIPIENT = gql`
  query GetSubscriptionRecipient($id: ID!) {
    subscriptionRecipient {
      get(id: $id) {
        id
        name
        address
        locality
        postalCode
        country
        phone
        email
        event
        eventText
        shippingDate
      }
    }
    message {
      allSubscriptionMessages {
        id
        title
        showField
        showCalendar
      }
    }
  }
`;

export default GET_SUBSCRIPTION_RECIPIENT;
