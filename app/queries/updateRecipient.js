import gql from 'graphql-tag';

const UPDATE_RECIPIENT = gql`
  mutation updateRecipient(
    $recipient: ID!
    $name: String!
    $address: String!
    $postalCode: String!
    $locality: String!
    $country: String!
    $phone: String!
    $email: String
  ) {
    recipient {
      update(
        recipient: $recipient
        input: {
          name: $name
          address: $address
          postalCode: $postalCode
          locality: $locality
          country: $country
          phone: $phone
          email: $email
        }
      ) {
        id
        name
        address
        postalCode
        locality
        country
        phone
        email
      }
    }
  }
`;

export default UPDATE_RECIPIENT;
