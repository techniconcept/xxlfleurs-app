import gql from 'graphql-tag';

const GET_ALL_SUBSCRIPTION_MESSAGES = gql`
  query GetAllSubscriptionMessages {
    message {
      allSubscriptionMessages {
        id
        title
        showField
        showCalendar
      }
    }
  }
`;

export default GET_ALL_SUBSCRIPTION_MESSAGES;
