import gql from 'graphql-tag';

const CREATE_RECIPIENT = gql`
  mutation createRecipient(
    $name: String!
    $address: String!
    $postalCode: String!
    $locality: String!
    $country: String!
    $phone: String!
    $email: String
  ) {
    recipient {
      create(
        input: {
          name: $name
          address: $address
          postalCode: $postalCode
          locality: $locality
          country: $country
          phone: $phone
          email: $email
        }
      ) {
        id
        name
        address
        postalCode
        locality
        country
        phone
        email
      }
    }
  }
`;

export default CREATE_RECIPIENT;
