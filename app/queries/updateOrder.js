import gql from 'graphql-tag';

const UPDATE_ORDER = gql`
  mutation updateOrder($order: ID!, $message: String, $shippingDate: DateTime) {
    order {
      update(
        order: $order
        input: { message: $message, shippingDate: $shippingDate }
      ) {
        id
        sku
        message
        shippingDate
        items {
          id
          name
        }
        recipient {
          id
          name
          postalCode
          locality
          address
          phone
        }
      }
    }
  }
`;

export default UPDATE_ORDER;
