import gql from 'graphql-tag';

const GET_RECIPIENTS = gql`
  query GetRecipient($id: ID!) {
    recipient {
      get(id: $id) {
        id
        name
        address
        locality
        postalCode
        country
        phone
        email
      }
    }
  }
`;

export default GET_RECIPIENTS;
