import gql from 'graphql-tag';

const CREATE_SUBSCRIPTION_RECIPIENT = gql`
  mutation createSubscriptionRecipient(
    $subscription: ID!
    $name: String!
    $address: String!
    $postalCode: String!
    $locality: String!
    $country: String!
    $phone: String!
    $email: String
    $event: String
    $eventText: String
    $shippingDate: DateTime
  ) {
    subscriptionRecipient {
      create(
        subscription: $subscription
        input: {
          name: $name
          address: $address
          postalCode: $postalCode
          locality: $locality
          country: $country
          phone: $phone
          email: $email
          event: $event
          eventText: $eventText
          shippingDate: $shippingDate
        }
      ) {
        id
        name
        address
        postalCode
        locality
        country
        phone
        email
        event
        eventText
        shippingDate
      }
    }
  }
`;

export default CREATE_SUBSCRIPTION_RECIPIENT;
