import gql from 'graphql-tag';

const GET_ALL_RECIPIENTS = gql`
  query GetAllRecipients {
    recipient {
      all {
        id
        name
        postalCode
        locality
        address
        phone
        country
      }
    }
  }
`;

export default GET_ALL_RECIPIENTS;
