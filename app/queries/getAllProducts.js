import gql from 'graphql-tag';

const GET_ALL_PRODUCTS = gql`
  query GetAllProducts {
    product {
      all {
        id
        name
        price
        donation
      }
    }
  }
`;

export default GET_ALL_PRODUCTS;
