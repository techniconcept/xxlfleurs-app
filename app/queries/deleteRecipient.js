import gql from 'graphql-tag';

const DELETE_RECIPIENT = gql`
  mutation deleteRecipient($recipient: ID!) {
    recipient {
      delete(recipient: $recipient)
    }
  }
`;

export default DELETE_RECIPIENT;
