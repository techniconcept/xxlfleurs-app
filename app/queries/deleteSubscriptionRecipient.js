import gql from 'graphql-tag';

const DELETE_SUBSCRIPTION_RECIPIENT = gql`
  mutation deleteSubscriptionRecipient($recipient: ID!) {
    subscriptionRecipient {
      delete(recipient: $recipient)
    }
  }
`;

export default DELETE_SUBSCRIPTION_RECIPIENT;
