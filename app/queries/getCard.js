import gql from 'graphql-tag';

const GET_CARD = gql`
  query GetCard {
    profile {
      card
    }
  }
`;

export default GET_CARD;
