/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import '@babel/polyfill';

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router/immutable';
import { PersistGate } from 'redux-persist/integration/react';
import history from 'utils/history';
import 'sanitize.css/sanitize.css';

// Import root app
import App from 'containers/App';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';

// Import Auth Provider
import AuthProvider from 'containers/AuthProvider';

// Load the favicon and the .htaccess file
/* eslint-disable import/no-unresolved, import/extensions */
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import 'file-loader?name=.htaccess!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

// import apollo
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';

import PullToRefresh from 'pulltorefreshjs';

import * as Sentry from '@sentry/browser';
import iPhoneInstallOverlay from './utils/iphone-install-overlay';

// Import i18n messages
import { translationMessages } from './i18n';

import configureStore from './configureStore';

import spritesURL from './images/mobile-sprite.png';
import appIconURL from './images/icon-512x512.png';

// create apollo client
const httpLink = createHttpLink({
  uri: process.env.GRAPHQL_API,
});

const logoutLink = onError(({ networkError }) => {
  if (networkError && networkError.statusCode !== 200) history.replace('/'); // TODO check this please !
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : '',
    },
  };
});

const client = new ApolloClient({
  link: logoutLink.concat(authLink.concat(httpLink)),
  cache: new InMemoryCache(),
});

// Create redux store with history
const initialState = {};
const { store, persistor } = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

if (process.env.NODE_ENV === 'production') {
  // Init Sentry
  Sentry.init({
    dsn: 'https://4406e4ba8ae948be8dc992b89fd1961b@sentry.io/1474396',
  });
}

PullToRefresh.init({
  mainElement: document.querySelector('section'),
  instructionsPullToRefresh: 'Tirer pour mettre à jour',
  instructionsReleaseToRefresh: 'Lâcher pour mettre à jour',
  instructionsRefreshing: 'Chargement',
  onRefresh() {
    PullToRefresh.destroyAll();
    window.location.reload();
  },
  shouldPullToRefresh() {
    return !window.scrollY;
  },
});

iPhoneInstallOverlay.init({
  showOnReload: false,
  blurElement: '#app',
  appIconURL,
  spritesURL,
  appName: 'XXL Fleurs',
  addText: "Ajouter à l'écran d'accueil",
  previewText: 'Fermer',
});

const render = messages => {
  ReactDOM.render(
    <ApolloProvider client={client}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AuthProvider>
            <LanguageProvider messages={messages}>
              <ConnectedRouter history={history}>
                <App />
              </ConnectedRouter>
            </LanguageProvider>
          </AuthProvider>
        </PersistGate>
      </Provider>
    </ApolloProvider>,
    MOUNT_NODE,
  );
};
if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n', 'containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  new Promise(resolve => {
    resolve(import('intl'));
  })
    .then(() => Promise.all([import('intl/locale-data/jsonp/en.js')]))
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  const runtime = require('offline-plugin/runtime'); // eslint-disable-line global-require
  runtime.install({
    onUpdateReady: () => {
      // Tells to new SW to take control immediately
      runtime.applyUpdate();
    },
    onUpdated: () => {
      // Reload the webpage to load into the new version
      window.location.reload();
    },
  });
}
