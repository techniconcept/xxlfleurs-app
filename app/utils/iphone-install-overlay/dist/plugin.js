// eslint-disable-next-line func-names
const iPhoneInstallOverlay = (function(document, localStorage) {
  const defaultConfig = {
    showOnReload: true,
    appIconURL: './images/cab.svg',
    spritesURL: './images/mobile-sprite.png',
    blurElement: '',
    appName: 'XXL Fleurs',
    addText: "Ajouter à l'écran d'accueil",
    previewText: 'Preview in browser',
  };

  const isiPhoneSafari =
    navigator.userAgent &&
    !navigator.userAgent.match('CriOS') &&
    navigator.userAgent.match(/iPhone/i);

  const isModeFullscreen =
    'standalone' in window.navigator && window.navigator.standalone !== true;

  // eslint-disable no-multi-str
  const DOM_TEMPLATE = `${'<div class="add-to-home">' +
    '<div class = "text-right browser-preview f12" ng-click="main.iPhoneNotFullscreen=false">{previewText}</div>' +
    '<div class = "logo-name-container" style="background-image:url({appIconURL})">{appName}' +
    ' </div>' +
    '<div class = "homescreen-text">Pour installer l\'application, cliquer sur' +
    ' <div class = "icon-addToHome sprite-mobile" style="background-image:url({spritesURL})"></div> et choisir' +
    ' <br /> {addText}' +
    '</div>' +
    ' <div class = "icon-homePointer `sprite-mobile"  style="background-image:url({spritesURL})"></div>' +
    ' </div'}`;

  function showOverlay() {
    // eslint-disable-next-line no-param-reassign
    document.querySelector('.add-to-home').style.display = 'block';
    document.querySelector(defaultConfig.blurElement).classList.add('blur');
  }

  function hideOverlay() {
    if (!defaultConfig.showOnReload) {
      localStorage.setItem('overlayStatus', 'hidden');
    }
    // eslint-disable-next-line no-param-reassign
    document.querySelector('.add-to-home').style.display = 'none';
    document.querySelector(defaultConfig.blurElement).classList.remove('blur');
  }

  function replaceString(obj, str) {
    let retStr = str;
    Object.keys(obj).forEach(key => {
      const regexPattern = `{${key}}`;
      retStr = retStr.replace(new RegExp(regexPattern, 'g'), obj[key]);
    });
    return retStr;
  }

  function initDom(config) {
    const dom = replaceString(config, DOM_TEMPLATE);
    const wrapper = document.createElement('div');
    wrapper.innerHTML = dom;
    document.body.appendChild(wrapper);
    if (isiPhoneSafari && isModeFullscreen) {
      if (
        !defaultConfig.showOnReload &&
        localStorage.getItem('overlayStatus') === 'hidden'
      ) {
        return;
      }
      showOverlay(config);
    }
  }

  function initClickEvents() {
    document
      .querySelector('.add-to-home .browser-preview')
      .addEventListener('click', hideOverlay);
  }

  return {
    init() {
      /* eslint-disable prefer-rest-params */
      const config =
        arguments.length <= 0 || arguments[0] === undefined
          ? defaultConfig
          : arguments[0];
      if (!config.blurElement) {
        // eslint-disable-next-line no-console
        console.error('Blur Element is required in config');
        return;
      }
      initDom(Object.assign(defaultConfig, config));
      initClickEvents();
    },
    hideOverlay,
    showOverlay,
  };
})(document, localStorage);

export default iPhoneInstallOverlay;
