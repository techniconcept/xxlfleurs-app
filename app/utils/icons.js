import React from 'react';
import styled from 'styled-components';

const SVG = styled.svg`
  flex: 1;
`;

function Checked() {
  return (
    <SVG
      aria-hidden="true"
      data-prefix="fas"
      data-icon="check-square"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
    >
      <path
        fill="currentColor"
        d="M400 480H48c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h352c26.51 0 48 21.49 48 48v352c0 26.51-21.49 48-48 48zm-204.686-98.059l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.248-16.379-6.249-22.628 0L184 302.745l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.25 16.379 6.25 22.628.001z"
      />
    </SVG>
  );
}

function Unchecked() {
  return (
    <SVG
      aria-hidden="true"
      data-prefix="fas"
      data-icon="square"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
    >
      <path
        fill="currentColor"
        d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48z"
      />
    </SVG>
  );
}

function HistoryIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="history"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M504 255.531c.253 136.64-111.18 248.372-247.82 248.468-59.015.042-113.223-20.53-155.822-54.911-11.077-8.94-11.905-25.541-1.839-35.607l11.267-11.267c8.609-8.609 22.353-9.551 31.891-1.984C173.062 425.135 212.781 440 256 440c101.705 0 184-82.311 184-184 0-101.705-82.311-184-184-184-48.814 0-93.149 18.969-126.068 49.932l50.754 50.754c10.08 10.08 2.941 27.314-11.313 27.314H24c-8.837 0-16-7.163-16-16V38.627c0-14.254 17.234-21.393 27.314-11.314l49.372 49.372C129.209 34.136 189.552 8 256 8c136.81 0 247.747 110.78 248 247.531zm-180.912 78.784l9.823-12.63c8.138-10.463 6.253-25.542-4.21-33.679L288 256.349V152c0-13.255-10.745-24-24-24h-16c-13.255 0-24 10.745-24 24v135.651l65.409 50.874c10.463 8.137 25.541 6.253 33.679-4.21z"
        className=""
      />
    </SVG>
  );
}

function TruckIcon() {
  return (
    <SVG
      aria-hidden="true"
      data-prefix="fas"
      data-icon="truck-moving"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 640 512"
    >
      <path
        fill="currentColor"
        d="M621.3 237.3l-58.5-58.5c-12-12-28.3-18.7-45.3-18.7H480V64c0-17.7-14.3-32-32-32H32C14.3 32 0 46.3 0 64v336c0 44.2 35.8 80 80 80 26.3 0 49.4-12.9 64-32.4 14.6 19.6 37.7 32.4 64 32.4 44.2 0 80-35.8 80-80 0-5.5-.6-10.8-1.6-16h163.2c-1.1 5.2-1.6 10.5-1.6 16 0 44.2 35.8 80 80 80s80-35.8 80-80c0-5.5-.6-10.8-1.6-16H624c8.8 0 16-7.2 16-16v-85.5c0-17-6.7-33.2-18.7-45.2zM80 432c-17.6 0-32-14.4-32-32s14.4-32 32-32 32 14.4 32 32-14.4 32-32 32zm128 0c-17.6 0-32-14.4-32-32s14.4-32 32-32 32 14.4 32 32-14.4 32-32 32zm272-224h37.5c4.3 0 8.3 1.7 11.3 4.7l43.3 43.3H480v-48zm48 224c-17.6 0-32-14.4-32-32s14.4-32 32-32 32 14.4 32 32-14.4 32-32 32z"
      />
    </SVG>
  );
}

function PlaneIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="paper-plane"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"
        className=""
      />
    </SVG>
  );
}

function UsersIcon() {
  return (
    <SVG
      aria-hidden="true"
      data-prefix="fas"
      data-icon="users"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 640 512"
    >
      <path
        fill="currentColor"
        d="M96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm448 0c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm32 32h-64c-17.6 0-33.5 7.1-45.1 18.6 40.3 22.1 68.9 62 75.1 109.4h66c17.7 0 32-14.3 32-32v-32c0-35.3-28.7-64-64-64zm-256 0c61.9 0 112-50.1 112-112S381.9 32 320 32 208 82.1 208 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zm-223.7-13.4C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z"
      />
    </SVG>
  );
}

function UserIcon() {
  return (
    <SVG
      aria-hidden="true"
      data-prefix="fas"
      data-icon="user"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 640 512"
    >
      <path
        fill="currentColor"
        d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"
      />
    </SVG>
  );
}

function EditIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="edit"
      className="svg-inline--fa fa-edit fa-w-18"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 576 512"
    >
      <path
        fill="currentColor"
        d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"
      />
    </SVG>
  );
}

function TrashIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="trash"
      className="svg-inline--fa fa-trash fa-w-14"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
    >
      <path
        fill="currentColor"
        d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"
      />
    </SVG>
  );
}

function AddIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="plus-square"
      className="svg-inline--fa fa-plus-square fa-w-14"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
    >
      <path
        fill="currentColor"
        d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-32 252c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92H92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"
      />
    </SVG>
  );
}

function RedoIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="redo"
      className="svg-inline--fa fa-redo fa-w-16"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M500.33 0h-47.41a12 12 0 0 0-12 12.57l4 82.76A247.42 247.42 0 0 0 256 8C119.34 8 7.9 119.53 8 256.19 8.1 393.07 119.1 504 256 504a247.1 247.1 0 0 0 166.18-63.91 12 12 0 0 0 .48-17.43l-34-34a12 12 0 0 0-16.38-.55A176 176 0 1 1 402.1 157.8l-101.53-4.87a12 12 0 0 0-12.57 12v47.41a12 12 0 0 0 12 12h200.33a12 12 0 0 0 12-12V12a12 12 0 0 0-12-12z"
      />
    </SVG>
  );
}

function ArrowLeftIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="far"
      data-icon="arrow-alt-circle-left"
      className="svg-inline--fa fa-arrow-alt-circle-left fa-w-16"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zm-72-20v40c0 6.6-5.4 12-12 12H256v67c0 10.7-12.9 16-20.5 8.5l-99-99c-4.7-4.7-4.7-12.3 0-17l99-99c7.6-7.6 20.5-2.2 20.5 8.5v67h116c6.6 0 12 5.4 12 12z"
      />
    </SVG>
  );
}

function ArrowRightIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="arrow-circle-right"
      className="svg-inline--fa fa-arrow-circle-right fa-w-16"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M256 8c137 0 248 111 248 248S393 504 256 504 8 393 8 256 119 8 256 8zm-28.9 143.6l75.5 72.4H120c-13.3 0-24 10.7-24 24v16c0 13.3 10.7 24 24 24h182.6l-75.5 72.4c-9.7 9.3-9.9 24.8-.4 34.3l11 10.9c9.4 9.4 24.6 9.4 33.9 0L404.3 273c9.4-9.4 9.4-24.6 0-33.9L271.6 106.3c-9.4-9.4-24.6-9.4-33.9 0l-11 10.9c-9.5 9.6-9.3 25.1.4 34.4z"
      />
    </SVG>
  );
}

function SignInAlt() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="sign-in-alt"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M416 448h-84c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h84c17.7 0 32-14.3 32-32V160c0-17.7-14.3-32-32-32h-84c-6.6 0-12-5.4-12-12V76c0-6.6 5.4-12 12-12h84c53 0 96 43 96 96v192c0 53-43 96-96 96zm-47-201L201 79c-15-15-41-4.5-41 17v96H24c-13.3 0-24 10.7-24 24v96c0 13.3 10.7 24 24 24h136v96c0 21.5 26 32 41 17l168-168c9.3-9.4 9.3-24.6 0-34z"
        className=""
      />
    </SVG>
  );
}

function SignOutAlt() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="sign-out-alt"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M497 273L329 441c-15 15-41 4.5-41-17v-96H152c-13.3 0-24-10.7-24-24v-96c0-13.3 10.7-24 24-24h136V88c0-21.4 25.9-32 41-17l168 168c9.3 9.4 9.3 24.6 0 34zM192 436v-40c0-6.6-5.4-12-12-12H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h84c6.6 0 12-5.4 12-12V76c0-6.6-5.4-12-12-12H96c-53 0-96 43-96 96v192c0 53 43 96 96 96h84c6.6 0 12-5.4 12-12z"
        className=""
      />
    </SVG>
  );
}

function XXLIcon() {
  return (
    <SVG viewBox="0 0 59 97" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <defs>
        <linearGradient
          x1="0.894243807%"
          y1="60.7400634%"
          x2="98.7569737%"
          y2="28.6169888%"
          id="linearGradient-1"
        >
          <stop stopColor="#925B14" offset="1%" />
          <stop stopColor="#FF6D2D" offset="97%" />
        </linearGradient>
        <linearGradient
          x1="20.458935%"
          y1="96.6792988%"
          x2="83.0766284%"
          y2="2.73094688%"
          id="linearGradient-2"
        >
          <stop stopColor="#F6BC05" offset="0%" />
          <stop stopColor="#EC8341" offset="100%" />
        </linearGradient>
      </defs>
      <g
        id="Page-1"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <path
          d="M35.08,8.72 C35.08,8.72 35.65,5.65 33.08,1.72 C30.77,0.22 28.36,-0.28 24.81,0.72 C19.9,2.07 18.96,4.16 18.67,4.94 C18.02,11.25 19.58,10.53 20.48,11.86 L20.42,11.79 C20.42,11.79 24,5.21 35.08,8.72 Z"
          id="path2397"
          fill="#FF6D2D"
          fillRule="nonzero"
        />
        <path
          d="M35.08,8.72 C35.08,8.72 35.65,5.65 33.08,1.72 C30.77,0.22 28.36,-0.28 24.81,0.72 C19.9,2.07 18.96,4.16 18.67,4.94 C18.02,11.25 19.58,10.53 20.48,11.86 L20.42,11.79 C20.42,11.79 24,5.21 35.08,8.72 Z"
          id="path2397-2"
          fill="url(#linearGradient-1)"
          fillRule="nonzero"
        />
        <path
          d="M33.06,1.69 C33.7938857,3.95266797 34.1881742,6.31165917 34.23,8.69 C35.12,27.04 17.47,49.92 4.92,67.86 C4.92,67.86 0.26,74.26 0.2,76.01 C0.14,77.76 1.2,80.23 3.91,85.35 C6.28,89.89 9.37,93.72 8.91,96.95 L32.35,50.44 C32.35,50.44 41.22,28.44 42.72,24 C44.22,19.56 45.16,13.09 39.63,7.65 C37.05,5.11 35.09,3 33.06,1.69 Z"
          id="path2399"
          fill="url(#linearGradient-2)"
          fillRule="nonzero"
        />
        <path
          d="M20.4,11.75 C20.4,11.75 19.33,8.29 18.65,4.9 C18.36,5.68 14.72,13.9 14.72,13.9 C13.1087887,17.6698921 12.9064877,21.893369 14.15,25.8 C20.8646129,45.8037687 30.6188636,64.6545665 43.07,81.69 C43.07,81.69 47.74,88.6 49.51,89.4 C49.51,89.4 57.81,76.96 58.99,71.79 C58.99,71.79 52.46,66.28 36.49,40.42 C20.52,14.56 20.4,11.75 20.4,11.75 Z"
          id="path2258"
          fill="#FF6D2D"
          fillRule="nonzero"
        />
      </g>
    </SVG>
  );
}

function SubscriptionIcon() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="address-card"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 576 512"
    >
      <path
        fill="currentColor"
        d="M528 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h480c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-352 96c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zm112 236.8c0 10.6-10 19.2-22.4 19.2H86.4C74 384 64 375.4 64 364.8v-19.2c0-31.8 30.1-57.6 67.2-57.6h5c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h5c37.1 0 67.2 25.8 67.2 57.6v19.2zM512 312c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-64c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-64c0 4.4-3.6 8-8 8H360c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16z"
      />
    </SVG>
  );
}

function StartIcon() {
  return (
    <SVG
      xmlns="http://www.w3.org/2000/svg"
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="play-circle"
      className="svg-inline--fa fa-play-circle fa-w-16"
      role="img"
      viewBox="0 0 512 512"
    >
      <path
        fill="currentColor"
        d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm115.7 272l-176 101c-15.8 8.8-35.7-2.5-35.7-21V152c0-18.4 19.8-29.8 35.7-21l176 107c16.4 9.2 16.4 32.9 0 42z"
      />
    </SVG>
  );
}

function UserPlus() {
  return (
    <SVG
      aria-hidden="true"
      focusable="false"
      data-prefix="fas"
      data-icon="user-plus"
      className="svg-inline--fa fa-user-plus fa-w-20"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 640 512"
    >
      <path
        fill="currentColor"
        d="M624 208h-64v-64c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v64h-64c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h64v64c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-64h64c8.8 0 16-7.2 16-16v-32c0-8.8-7.2-16-16-16zm-400 48c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"
      />
    </SVG>
  );
}

export function ChooseIcon(icon) {
  switch (icon) {
    case 'truck':
      return TruckIcon();
    case 'history':
      return HistoryIcon();
    case 'plane':
      return PlaneIcon();
    case 'users':
      return UsersIcon();
    case 'user':
      return UserIcon();
    case 'edit':
      return EditIcon();
    case 'trash':
      return TrashIcon();
    case 'add':
      return AddIcon();
    case 'redo':
      return RedoIcon();
    case 'arrow-left':
      return ArrowLeftIcon();
    case 'arrow-right':
      return ArrowRightIcon();
    case 'sign-in-alt':
      return SignInAlt();
    case 'sign-out-alt':
      return SignOutAlt();
    case 'xxl':
      return XXLIcon();
    case 'checked':
      return Checked();
    case 'unchecked':
      return Unchecked();
    case 'subscription':
      return SubscriptionIcon();
    case 'start':
      return StartIcon();
    case 'user-plus':
      return UserPlus();
    default:
      return null;
  }
}
