import auth0 from 'auth0-js';

const auth0Client = new auth0.WebAuth({
  domain: process.env.AUTH0_DOMAIN,
  clientID: process.env.AUTH0_CLIENT_ID,
  redirectUri: process.env.REDIRECT_URL,
  responseType: 'token id_token',
  scope: 'openid profile email',
});

export const handleAuthentication = () =>
  new Promise((resolve, reject) => {
    auth0Client.parseHash((err, authResult) =>
      resolveAuthResult(err, authResult, resolve, reject),
    );
  });

export const checkSession = () =>
  new Promise((resolve, reject) => {
    auth0Client.checkSession({}, (err, authResult) =>
      resolveAuthResult(err, authResult, resolve, reject),
    );
  });

/* eslint-disable no-param-reassign */
export const signIn = (options = {}) => {
  clearPersistedToken();
  options.language = 'fr';
  auth0Client.authorize(options);
};

export const signOut = () => {
  clearPersistedToken();
  auth0Client.logout({
    returnTo: process.env.REDIRECT_URL,
    clientID: process.env.AUTH0_CLIENT_ID,
  });
};

export const clearPersistedToken = () => {
  localStorage.removeItem('token');
};
export const persistToken = token => localStorage.setItem('token', token);

const resolveAuthResult = (err, authResult, resolve, reject) => {
  if (err) return reject(err);
  if (!authResult || !authResult.idToken) {
    return reject(err);
  }

  const { idToken, idTokenPayload } = authResult;

  // set the time that the id token will expire at
  const { picture, name, nickname, email, exp } = idTokenPayload;
  const expiresAt = exp * 1000;

  clearPersistedToken();

  return resolve({
    authenticated: true,
    idToken,
    name,
    nickname,
    email,
    picture,
    expiresAt,
  });
};
