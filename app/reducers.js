/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { persistCombineReducers } from 'redux-persist-immutable';
import { connectRouter } from 'connected-react-router/immutable';
import storage from 'redux-persist/lib/storage';

import history from 'utils/history';
import authProviderReducer from 'containers/AuthProvider/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import newOrderReducer from 'containers/NewOrder/reducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['newOrder'],
};

/**
 * Merges the main reducer with the router state and dynamically injected reducers
 */
export default function createReducer(injectedReducers = {}) {
  const rootReducer = persistCombineReducers(persistConfig, {
    auth: authProviderReducer,
    newOrder: newOrderReducer,
    language: languageProviderReducer,
    ...injectedReducers,
  });

  // Wrap the root reducer and return a new root reducer with router state
  const mergeWithRouterState = connectRouter(history);
  return mergeWithRouterState(rootReducer);
}
