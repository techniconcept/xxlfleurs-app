import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Ubuntu', sans-serif;
    background-color: #f2f2f2;
  }

  body.fontLoaded {
    font-family: 'Ubuntu', sans-serif;
  }

  /* CSS reset with box-sizing fix */
  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed,
  figure, figcaption, footer, header, hgroup,
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
   margin: 0;
   padding: 0;
   border: 0;
   font-size: 100%;
   font-family: 'Ubuntu', sans-serif;
   vertical-align: baseline;
  }
  article, aside, details, figcaption, figure,
  footer, header, hgroup, menu, nav, section {
   display: block;
  }
  html {
   box-sizing: border-box;
  }
  *, *:before, *:after {
   box-sizing: inherit;
  }
  body {
   line-height: 1;
  }
  ol, ul {
   list-style: none;
  }
  blockquote, q {
   quotes: none;
  }
  blockquote:before, blockquote:after,
  q:before, q:after {
   content: '';
   content: none;
  }
  table {
   border-collapse: collapse;
   border-spacing: 0;
  }
  .react-calendar {
    width: 100%!important;
  }
  
  .StripeElement {
    margin-bottom: 30px;
    box-sizing: border-box;
    height: 40px;
    padding: 10px 12px;
    border: 1px solid gray;
    border-radius: 4px;
    background-color: transparent;
  
    -webkit-transition: box-shadow 150ms ease;
    transition: box-shadow 150ms ease;
  }

  .StripeElement--invalid {
    border-color: #fa755a;
  }

  .StripeElement--webkit-autofill {
    background-color: #fefde5 !important;
  }
  
  .add-to-home {
  display: none;
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  text-align: center;
  color: #fff;
  padding: 7vh 5vw 10vh 5vw;
  box-sizing: border-box;
  background-color: #000;
  z-index: 9999;
  background-color: rgba(0, 0, 0, 0.3);
}
.blur {
  filter: blur(10px);
  -webkit-filter: blur(10px);
  transition: 0.2s filter linear;
  -webkit-transition: 0.2s -webkit-filter linear;
}
.add-to-home .browser-preview {
  margin: -5px 0 40px;
  text-decoration: underline;
  opacity: 0.8;
  text-align: right;
}
.add-to-home .logo-name-container {
  background-repeat: no-repeat;
  background-position: center 0;
  padding-top: 155px;
  margin: 0 45px;
  background-size: 125px;
  font-size: 24px;
  margin-top: 15vh;
}
.add-to-home .homescreen-text {
  padding-top: 7vh;
  line-height: 1.5;
  font-size: 18px;
}
.add-to-home .icon-addToHome {
  vertical-align: text-bottom;
  width: 35px;
  height: 35px;
  display: inline-block;
  background-size: cover;
}
.add-to-home .icon-homePointer {
  margin-top: 5vh;
  background-position: center -40px;
  width: 100%;
  height: 50px;
  background-size: 40px;
  -webkit-animation-duration: 0.5s;
  animation-duration: 0.5s;
  -webkit-animation-name: topToBottom;
  animation-name: topToBottom;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}
@keyframes topToBottom {
  from {
    transform: translate(0, 0);
  }
  to {
    transform: translate(0, 20px);
  }
}

`;

export default GlobalStyle;
