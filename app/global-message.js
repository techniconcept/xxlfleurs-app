/*
 * App Messages
 *
 * TODO: Check for i18n
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app';

export default defineMessages({
  newOrder: 'Nouvelle livraison',
  everyWhere: 'Partout dans le monde',
  orders: 'Mes livraisons',
  subscription: 'Abonnement',
  subscriptionText: '4 bouquets par année',
  recipients: 'Mes destinataires',
  login: 'Connexion',
  account: 'Mon compte',
  sendToFriend: 'Envoyer à un ami',
  update: 'Modifier',
  create: 'Créer',
  name: 'Votre nom et prénom',
  address: 'Votre adresse',
  postalCode: 'Votre code postal',
  locality: 'Votre localité',
  country: 'Votre pays',
  phone: 'Votre téléphone / mobile',
  mobile: 'Votre téléphone mobile',
  email: 'Votre courrier électronique',
  customer: {},
  recipient: {
    name: 'Nom et prénom du destinataire',
    address: 'Adresse du destinataire',
    postalCode: 'Code postal du destinataire',
    locality: 'Localité du destinataire',
    country: 'Pays du destinataire',
    phone: 'Téléphone / Mobile du destinataire',
    mobile: 'Téléphone mobile du destinataire',
    email: 'Courrier électronique du destinataire',
  },
});
