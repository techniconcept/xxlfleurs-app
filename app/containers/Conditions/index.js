/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import styled, { css } from 'styled-components';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';

import messages from './messages';
import {
  PageHeader,
  Header,
  PageWrapper,
  Menu,
  Section,
  Footer,
} from '../../components';

const Item = styled.div`
  display: block;
  font-family: 'Ubuntu', sans-serif;
  font-size: 14px;
  line-height: 1.25;
  color: #656565;
  padding: 0 25px;
  margin: 25px 0;

  ${props =>
    props.title &&
    css`
      font-weight: bold;
    `};
`;
/* eslint-disable react/prefer-stateless-function */
export class Conditions extends React.PureComponent {
  render() {
    const { isAuthenticated } = this.props;

    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center>
              <FormattedMessage {...messages.header} />
            </Header>
          </div>
        </PageHeader>
        <Section>
          <Item title>1. Objet</Item>
          <Item>
            <strong>1.1</strong> La présente politique de confidentialité a pour
            objet d’informer les utilisateurs sur la nature des données
            personnelles collectées et traitées par XXL Fleurs, ainsi que sur la
            finalité de ce traitement et les droits des utilisateurs vis-à-vis
            des données personnelles recueillies.
          </Item>
          <Item>
            1.2 La présente politique s’applique en sus des conditions générales
            et des autres documents compris dans la Documentation contractuelle.
            En cas de divergence entre les conditions générales et les présentes
            conditions, ces dernières l’emportent.
          </Item>
          <Item>
            1.3 En cas de désaccord avec la présente politique, l’utilisateur
            est invité à ne pas utiliser le Site ou l’App et à ne pas
            communiquer ses données personnelles.
          </Item>
          <Item title>2. Données collectées</Item>
          <Item>
            2.1 Informations collectées automatiquement. En consultant le Site
            ou l’App certaines données concernant l’utilisateur sont collectées
            automatiquement (adresse IP, localisation, pages de la Plateforme
            consultées, date et heure de consultation, préférence de
            l’utilisateur, système d’exploitation, données d’accès, etc.). XXL
            Fleurs n’utilise pas de cookies. Certains procédés similaires aux
            cookies sont utilisés sur les terminaux mobiles. Les données
            récoltées automatiquement dans ce cadre sont traitées conformément à
            la présente politique de confidentialité.
          </Item>
          <Item>
            2.2 Informations fournies librement par les utilisateurs.
            l’utilisateur peut être amené à fournir des données personnelles le
            concernant à XXL Fleurs. Les données nécessaires à l’inscription sur
            l’application sont uniquement le numéro de mobile. Ces données sont
            obligatoires et leur absence peut rendre impossible l’inscription
            sur la plateforme.
          </Item>
          <Item>
            Au-delà des données nécessaires, des données optionnelles peuvent
            être fournies volontairement par l’utilisateur, par exemple: nom,
            prénom, date de naissance.
          </Item>
          <Item title>3. Utilisation des données collectées</Item>
          <Item>
            3.1 Données collectées automatiquement. Les données collectées
            automatiquement permettent à XXL Fleurs d’analyser le comportement
            des utilisateurs sur le Site et l’App, le trafic et les préférences,
            notamment afin d’optimiser et d’améliorer la qualité de ses
            Services.
          </Item>
          <Item>
            3.2 Les données fournies par les utilisateurs ou des tiers. XXL
            Fleurs traite et exploite les données nécessaires afin de remplir
            ses obligations contractuelles (envers ses utilisateurs et ses
            Clients) et vérifier le respect des conditions d’utilisation. Le
            traitement de ces données permet également de fournir un contenu
            personnalisé pour mieux répondre aux attentes de l’utilisateur.
          </Item>
          <Item title>4. Communication et transmission des données</Item>
          <Item>
            4.1 Les données personnelles de l’utilisateur (par exemple: numéro
            de téléphone ou IBAN) ne sont pas transmises à des tiers avec
            l’intention de les utiliser à des fins de marketing direct, sauf
            autorisation contraire de l’utilisateur.
          </Item>
          <Item title>5. Droits des utilisateurs</Item>
          <Item>
            5.1 Droit d’accès. l’utilisateur peut à tout moment demander à
            accéder à ses données personnelles stockées par XXL Fleurs.
          </Item>
          <Item>
            5.2 Droit de rectification. l’utilisateur dont les données sont
            inexactes ou incomplètes a le droit d’obtenir leur rectification.
          </Item>
          <Item>
            5.3 Droit de rétractation et/ ou opposition. l’utilisateur peut à
            tout moment s’opposer à la collecte ou au traitement de certaines
            données le concernant (opting out). Cette opposition peut entraîner
            la cessation de l’utilisation du Site, de l’App ou la fourniture de
            Services.
          </Item>
          <Item>
            5.4 Droit à la suppression. l’utilisateur a le droit de requérir
            l’effacement de(s) données personnelles le concernant lorsque leur
            utilisation n’est plus nécessaire ou que l’utilisateur n’est plus
            d’accord avec le traitement de ses données. Ces données seront
            supprimées dans les meilleurs délais, sauf disposition légale
            contraire ou qu’un intérêt légitime de XXL Fleurs s’y oppose. Il est
            précisé, qu’à défaut de demande expresse de suppression, les données
            transmises par l’utilisateur sont conservées (conformément à l’art.
            7) même en cas de désactivation du compte Utilisateur.
          </Item>
          <Item>
            5.5 Exercice des droits. Pour exercer les droits ci-dessus,
            l’utilisateur peut faire une demande à tout moment à l’adresse
            suivante : famine@xxlfondation.ch
          </Item>
          <Item title>6. Sécurité</Item>
          <Item>
            6.1 XXL Fleurs prend la protection des données de ses utilisateurs
            très au sérieux et s’engage à traiter leurs données personnelles de
            manière confidentielle et avec la plus grande précaution. XXL Fleurs
            prend notamment les mesures de sécurité appropriées afin d’empêcher
            l’accès, la divulgation, la modification ou la destruction non
            autorisées des données collectées conformément aux art. 7 LPD et 8 à
            9 de l’ordonnance relative à la loi fédérale sur la protection des
            données (OLPD). Toutefois XXL Fleurs ne peut garantir une protection
            complète et intégrale des données contre les traitements non
            autorisés en raison des risques liés à l’utilisation d’internet et
            des moyens de communication électronique.
          </Item>
          <Item title>7. Conservation des données</Item>
          <Item>
            7.1 Les données collectées par XXL Fleurs sont conservées aussi
            longtemps que la finalité pour laquelle elles ont été récoltées le
            requiert, sous réserve de dispositions légales prévoyant des
            périodes de conservation des données plus longues. Passé ce délai,
            les données personnelles sont supprimées.
          </Item>
          <Item title>8. Modifications</Item>
          <Item>
            8.1 XXL Fleurs se réserve le droit de modifier sa politique de
            confidentialité à tout moment. Les modifications seront publiées sur
            le Site et/ou l’App et entreront en vigueur dès la date de mise en
            ligne.
          </Item>
          <Item>
            8.2 La politique de confidentialité actuellement en ligne est
            applicable. En poursuivant la navigation sur le Site et/ou l’App
            l’utilisateur accepte la politique de confidentialité en vigueur.
          </Item>
        </Section>
        <Footer />
      </PageWrapper>
    );
  }
}

Conditions.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createSelector(
  makeSelectIsAuthenticated(),
  isAuthenticated => ({
    isAuthenticated,
  }),
);

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Conditions);
