/**
 *
 * Asynchronously loads the component for ProfileManagement
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
