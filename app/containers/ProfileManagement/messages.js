/*
 * ProfileManagement Messages
 *
 * This contains all the text for the ProfileManagement container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ProfileManagement';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Mon compte',
  },
});
