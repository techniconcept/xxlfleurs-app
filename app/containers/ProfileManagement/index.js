/**
 *
 * ProfileManagement
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';

import { Query } from 'react-apollo';
import messages from './messages';
import {
  Section,
  Footer,
  Header,
  Menu,
  PageWrapper,
  PageHeader,
  BigButton,
  UserInfo,
  ActivityIndicator,
} from '../../components';
import { GET_PROFILE } from '../../queries';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';
import { signOut } from '../../utils/auth';

/* eslint-disable react/prefer-stateless-function */
export class ProfileManagement extends React.Component {
  render() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated === false) return <Redirect to="/" />;

    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center>
              <FormattedMessage {...messages.header} />
            </Header>
          </div>
        </PageHeader>

        <Section>
          <Query query={GET_PROFILE}>
            {({ loading, error, data }) => {
              if (loading) return <ActivityIndicator loading={loading} />;
              if (error) return <p>{error}</p>;
              return (
                <div>
                  <UserInfo
                    name={data.profile.current.name}
                    address={data.profile.current.address}
                    locality={data.profile.current.locality}
                    phone={data.profile.current.phone}
                    postalCode={data.profile.current.postalCode}
                    mobile={data.profile.current.mobile}
                    country={data.profile.current.country}
                  />
                  <BigButton
                    headline="Mes commandes"
                    href="/commandes"
                    icon="truck"
                  />
                  <BigButton
                    headline="Mes destinataires"
                    icon="users"
                    href="/destinataires"
                  />
                  <BigButton
                    headline="Déconnexion"
                    icon="sign-out-alt"
                    onClick={() => {
                      if (
                        // eslint-disable-next-line no-alert
                        window.confirm(
                          'Êtes-vous sûr de vouloir vous déconnecter ?',
                        )
                      ) {
                        signOut();
                      }
                    }}
                  />
                </div>
              );
            }}
          </Query>
        </Section>

        <Footer />
      </PageWrapper>
    );
  }
}

ProfileManagement.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ProfileManagement);
