import { fromJS } from 'immutable';
import profileManagementReducer from '../reducer';

describe('profileManagementReducer', () => {
  it('returns the initial state', () => {
    expect(profileManagementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
