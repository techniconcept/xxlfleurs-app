/**
 *
 * Share
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import QRCode from 'qrcode.react';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import styled from 'styled-components';
import reducer from './reducer';
import saga from './saga';
import {
  Footer,
  Header,
  Menu,
  PageHeader,
  PageWrapper,
} from '../../components';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';

const QRCodeWrapper = styled.div`
  text-align: center;
`;

const TextWrapper = styled.div`
  text-align: center;
  margin-top: 1em;
  padding: 0 2em;
`;

const Copyright = styled.div`
  text-align: center;
  margin-top: 3em;
  padding: 0 2em;
  line-height: 1.3em;
`;

const ExternalLink = styled.a`
  color: rgba(255, 112, 0, 1);
  text-decoration: none;
`;

/* eslint-disable react/prefer-stateless-function */
export class Share extends React.PureComponent {
  render() {
    const { isAuthenticated } = this.props;
    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center>Inviter des amis</Header>
          </div>
        </PageHeader>
        <QRCodeWrapper>
          <QRCode value="http://l.ead.me/bb5OWO" />
        </QRCodeWrapper>
        <TextWrapper>
          {"Faites scanner ce QR-code à vos connaissances, afin qu'ils aient\n" +
            "          également accès à l'app"}
        </TextWrapper>
        <Copyright>
          Made with{' '}
          <span role="img" aria-label="coeur">
            ❤️
          </span>{' '}
          <span role="img" aria-label="fleur">
            🌼️
          </span>
          <br />
          by{' '}
          <ExternalLink href="https://www.techniconcept.ch" target="_blank">
            techniconcept.ch
          </ExternalLink>{' '}
          /{' '}
          <ExternalLink href="https://ttree.ch/" target="_blank">
            ttree.ch
          </ExternalLink>
        </Copyright>
        <Footer />
      </PageWrapper>
    );
  }
}

Share.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'share', reducer });
const withSaga = injectSaga({ key: 'share', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Share);
