/**
 *
 * Asynchronously loads the component for Share
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
