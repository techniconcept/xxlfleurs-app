/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'XXL Fondation',
  },
  introduction: {
    id: `${scope}.introduction`,
    defaultMessage:
      'Aidez-nous à lutter contre la malnutrition et la famine en offrant des fleurs ; le bénéfice va entièrement à cette cause.',
  },
});
