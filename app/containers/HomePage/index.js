/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import iPhoneInstallOverlay from 'iphone-install-overlay';
import styled from 'styled-components';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';
import {
  PageWrapper,
  Header,
  Paragraph,
  PageHeader,
  BigButton,
  Footer,
  Logo,
} from '../../components';
import messages from './messages';
import globalMessages from '../../global-message';
import { resetOrder } from '../NewOrder/actions';
import history from '../../utils/history';
import Route from '../../route.config';
import { signIn } from '../../utils/auth';
import Button from '../../components/Button';
import welcome from './welcome.png';

const H2 = styled.h2`
  text-transform: uppercase;
  margin-bottom: 15px;
  color: rgba(255, 112, 0, 1);
  font-size: 22px;
`;
const P = styled.p`
  font-size: 16px;
  line-height: 18px;
  color: #333333;
`;

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends React.PureComponent {
  toNewOrder() {
    const { dispatch } = this.props;
    dispatch(resetOrder());
    history.push(Route.ORDER);
  }

  render() {
    const { isAuthenticated } = this.props;
    return (
      <PageWrapper>
        <PageHeader>
          <div>
            <Logo compact />
            <Header center big>
              <FormattedMessage {...messages.header} />
            </Header>
            <Paragraph center teaser>
              <FormattedMessage {...messages.introduction} />
            </Paragraph>
          </div>
        </PageHeader>
        {navigator.userAgent.match(/iPhone/i) ? (
          <Button center onClick={() => iPhoneInstallOverlay.showOverlay()}>
            {"Ajouter à l'écran d'accueil"}
          </Button>
        ) : null}

        <div style={{ padding: '20px', backgroundColor: 'white' }}>
          <H2>
            Offrez
            <br /> des fleurs en 5 clics
          </H2>
          <BigButton
            headline={globalMessages.newOrder}
            text={globalMessages.everyWhere}
            icon="truck"
            onClick={() => this.toNewOrder()}
          />
          {isAuthenticated ? (
            <BigButton
              headline={globalMessages.subscription}
              text={globalMessages.subscriptionText}
              icon="subscription"
              onClick={() => {
                window.scrollTo(0, 0);
                history.push('/subscription');
              }}
            />
          ) : null}
          {isAuthenticated === false ? (
            <BigButton
              headline={globalMessages.login}
              icon="sign-in-alt"
              onClick={() => signIn()}
            />
          ) : null}
          {isAuthenticated ? (
            <BigButton
              headline={globalMessages.orders}
              icon="history"
              href="/commandes"
            />
          ) : null}
          {isAuthenticated ? (
            <BigButton
              headline={globalMessages.recipients}
              icon="users"
              href="/destinataires"
            />
          ) : null}
          {isAuthenticated ? (
            <BigButton
              headline={globalMessages.account}
              icon="user"
              href="/compte"
            />
          ) : null}
          <BigButton
            headline={globalMessages.sendToFriend}
            icon="plane"
            href="/share"
          />
          <P>
            {"Depuis 2014, la XXL Fondation, reconnue d'utilité publique, "}
            <b>lutte contre la malnutrition et la famine</b> dans le monde et
            plus spécifiquement au Burkina Faso. <br /> <br />
          </P>
          <P>
            Dans ce but, cette application permet{' '}
            <b>d’offrir des fleurs, livrées partout dans le monde</b> et dont
            les bénéfices sont utilisés pour cette cause.
          </P>
          <img
            style={{ width: '100%', marginTop: '20px', marginBottom: '20px' }}
            src={welcome}
            alt="Offrez des fleurs en 5 clics"
          />
        </div>
        <Footer />
      </PageWrapper>
    );
  }
}

HomePage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createSelector(
  makeSelectIsAuthenticated(),
  isAuthenticated => ({
    isAuthenticated,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
