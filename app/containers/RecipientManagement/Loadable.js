/**
 *
 * Asynchronously loads the component for RecipientManagement
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
