import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';

import styled from 'styled-components';
import Mutation from 'react-apollo/Mutation';
import { FormattedMessage } from 'react-intl';
import { GET_ALL_RECIPIENTS, DELETE_RECIPIENT } from '../../../queries';
import ActivityIndicator from '../../../components/ActivityIndicator';
import Recipient from '../../../components/Recipient';
import messages from '../messages';
import Paragraph from '../../../components/Paragraph';

const RecipientWrapper = styled.div`
  padding: 15px;
  &:nth-child(even) {
    background: #fff;
  }
`;

// eslint-disable-next-line func-names
const List = function(props) {
  return (
    <div>
      <Query query={GET_ALL_RECIPIENTS}>
        {({ loading, error, data }) => {
          if (loading) return <ActivityIndicator loading={loading} />;
          if (error) return <p>{error}</p>;

          if (!data.recipient.all.length) {
            return (
              <div>
                <Paragraph center>
                  <FormattedMessage {...messages.noRecipients} />
                </Paragraph>
              </div>
            );
          }
          return data.recipient.all.map(
            ({ id, name, postalCode, address, locality, phone, country }) => (
              <RecipientWrapper key={id}>
                <Mutation
                  mutation={DELETE_RECIPIENT}
                  onCompleted={props.onMutationCompleted}
                  onError={props.onMutationError}
                  update={cache => {
                    const allRecipients = cache.readQuery({
                      query: GET_ALL_RECIPIENTS,
                    });
                    cache.writeQuery({
                      query: GET_ALL_RECIPIENTS,
                      data: {
                        recipient: {
                          all: allRecipients.recipient.all.filter(
                            e => e.id !== id,
                          ),
                        },
                      },
                    });
                  }}
                >
                  {deleteRecipient => (
                    <Recipient
                      name={name}
                      postalCode={postalCode}
                      address={address}
                      locality={locality}
                      phone={phone}
                      country={country}
                      context="edition"
                      onClick={() => {
                        props.onEditClick(id);
                      }}
                      onDeleteClick={props.onDeleteClick(deleteRecipient, id)}
                    />
                  )}
                </Mutation>
              </RecipientWrapper>
            ),
          );
        }}
      </Query>
    </div>
  );
};

List.propTypes = {
  onEditClick: PropTypes.func.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
  onMutationCompleted: PropTypes.func,
  onMutationError: PropTypes.func,
};

export default List;
