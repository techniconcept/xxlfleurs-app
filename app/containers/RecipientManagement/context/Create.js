import React from 'react';
import PropTypes from 'prop-types';

import RecipientCreateForm from '../form/create';

// eslint-disable-next-line func-names
const Create = function(props) {
  return <RecipientCreateForm goToList={props.goToList} />;
};

Create.propTypes = {
  goToList: PropTypes.func.isRequired,
};

export default Create;
