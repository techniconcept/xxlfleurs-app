/**
 *
 * RecipientManagement
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';

import messages from './messages';
import {
  PageHeader,
  Header,
  Section,
  Footer,
  PageWrapper,
  Menu,
  Icon,
} from '../../components';
import List from './context/List';
import Edit from './context/Edit';
import Create from './context/Create';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';

const CONTEXT_LIST = 'context_list';
const CONTEXT_EDIT = 'context_edit';
const CONTEXT_CREATE = 'context_create';

/* eslint-disable react/prefer-stateless-function */
export class RecipientManagement extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      context: CONTEXT_LIST,
      id: null,
    };
  }

  getTitle() {
    switch (this.state.context) {
      case CONTEXT_LIST:
        return <FormattedMessage {...messages.headerList} />;
      case CONTEXT_EDIT:
        return <FormattedMessage {...messages.headerEdit} />;
      case CONTEXT_CREATE:
        return <FormattedMessage {...messages.headerCreate} />;
      default:
        return '';
    }
  }

  getNavigation() {
    if (
      this.state.context === CONTEXT_EDIT ||
      this.state.context === CONTEXT_CREATE
    ) {
      return <Icon onClick={() => this.goToList()} left icon="arrow-left" />;
    }

    return (
      <Icon
        icon="add"
        right
        onClick={() => this.setState({ context: CONTEXT_CREATE })}
      />
    );
  }

  getSection() {
    switch (this.state.context) {
      case CONTEXT_LIST:
        return (
          <div>
            <List
              // TODO : Handle error
              onEditClick={id => this.setState({ id, context: CONTEXT_EDIT })}
              onDeleteClick={(deleteRecipient, id) => () => {
                deleteRecipient({
                  variables: {
                    recipient: id,
                  },
                });
              }}
            />
          </div>
        );
      case CONTEXT_EDIT:
        return (
          <div>
            <Edit
              idRecipient={this.state.id}
              goToList={() => this.goToList()}
            />
          </div>
        );
      case CONTEXT_CREATE:
        return <Create goToList={() => this.goToList()} />;
      default:
        return '';
    }
  }

  goToList() {
    this.setState({ context: CONTEXT_LIST });
  }

  render() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated === false) return <Redirect to="/" />;

    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center>
              {this.getNavigation()}
              {this.getTitle()}
            </Header>
          </div>
        </PageHeader>

        <Section>{this.getSection()}</Section>

        <Footer />
      </PageWrapper>
    );
  }
}

RecipientManagement.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(RecipientManagement);
