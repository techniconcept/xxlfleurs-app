import { fromJS } from 'immutable';
import recipientManagementReducer from '../reducer';

describe('recipientManagementReducer', () => {
  it('returns the initial state', () => {
    expect(recipientManagementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
