/**
 *
 * RecipientEditForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import Mutation from 'react-apollo/Mutation';
import {
  Button,
  ActivityIndicator,
  FormInput,
  CountrySelect,
} from '../../../components';
import globalMessages from '../../../global-message';
import { UPDATE_RECIPIENT, GET_RECIPIENT } from '../../../queries';

/* eslint-disable react/prefer-stateless-function */
class RecipientEditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      hasEmptyCountry: false,
    };
    this.goToList = props.goToList;
  }

  handleInputChange(event) {
    const { target } = event;
    const { value, name } = target;

    this.setState({
      [name]: value,
    });
  }

  render() {
    const params = { id: this.state.id };
    return (
      <Mutation
        mutation={UPDATE_RECIPIENT}
        update={() => {
          this.props.goToList();
        }}
        ignoreResults
      >
        {updateRecipient => (
          <Query query={GET_RECIPIENT} variables={params}>
            {({ loading, error, data }) => {
              if (loading) return <ActivityIndicator loading={loading} />;
              if (error) return <p>{error}</p>;
              const recipient = Object.assign(data.recipient.get, this.state);
              const {
                name,
                address,
                postalCode,
                locality,
                country,
                phone,
                email,
              } = recipient;
              const { hasEmptyCountry } = this.state;
              return (
                <div>
                  <form
                    onSubmit={e => {
                      e.preventDefault();
                      this.setState({ hasEmptyCountry: country === null });
                      if (!country) {
                        return;
                      }
                      updateRecipient({
                        variables: {
                          name,
                          address,
                          postalCode,
                          locality,
                          country,
                          phone,
                          email,
                          recipient: this.state.id,
                        },
                      });
                    }}
                  >
                    <FormInput
                      label={globalMessages.recipient.name}
                      name="name"
                      value={name}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.address}
                      name="address"
                      value={address}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.postalCode}
                      name="postalCode"
                      value={postalCode}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.locality}
                      name="locality"
                      value={locality}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <CountrySelect
                      label={globalMessages.recipient.country}
                      name="country"
                      current={country}
                      onChange={({ value }) => {
                        if (value !== '') {
                          this.setState({
                            country: value,
                            hasEmptyCountry: false,
                          });
                        } else {
                          this.setState({
                            country: null,
                            hasEmptyCountry: true,
                          });
                        }
                      }}
                      required
                      error={hasEmptyCountry}
                    />
                    <FormInput
                      type="tel"
                      label={globalMessages.recipient.phone}
                      name="phone"
                      value={phone}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      type="email"
                      label={globalMessages.recipient.email}
                      name="email"
                      value={email}
                      onChange={e => this.handleInputChange(e)}
                    />
                    <Button center>{globalMessages.update}</Button>
                  </form>
                </div>
              );
            }}
          </Query>
        )}
      </Mutation>
    );
  }
}

RecipientEditForm.propTypes = {
  id: PropTypes.string.isRequired,
  goToList: PropTypes.func.isRequired,
};

export default RecipientEditForm;
