/**
 *
 * RecipientCreateForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Mutation from 'react-apollo/Mutation';
import { FormInput, CountrySelect, Button } from '../../../components';
import globalMessages from '../../../global-message';
import { CREATE_RECIPIENT, GET_ALL_RECIPIENTS } from '../../../queries';

/* eslint-disable react/prefer-stateless-function */
class RecipientCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.goToList = props.goToList;
    this.state = {
      hasEmptyCountry: false,
    };
  }

  handleInputChange(event) {
    const { target } = event;
    const { value, name } = target;

    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <Mutation
        mutation={CREATE_RECIPIENT}
        update={(cache, { data: { recipient } }) => {
          const data = cache.readQuery({
            query: GET_ALL_RECIPIENTS,
          });
          data.recipient.all.push(recipient.create);
          cache.writeQuery({
            query: GET_ALL_RECIPIENTS,
            data,
          });
          if (this.props.goToList) {
            this.props.goToList();
          }
        }}
      >
        {createRecipient => {
          const {
            name,
            address,
            postalCode,
            locality,
            country,
            phone,
            email,
            hasEmptyCountry,
          } = this.state;
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                this.setState({ hasEmptyCountry: country === null });
                if (!country) {
                  return;
                }
                createRecipient({
                  variables: {
                    name,
                    address,
                    postalCode,
                    locality,
                    country,
                    phone,
                    email,
                  },
                });
              }}
            >
              <FormInput
                label={globalMessages.recipient.name}
                name="name"
                value={name}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.address}
                name="address"
                value={address}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.postalCode}
                name="postalCode"
                value={postalCode}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.locality}
                name="locality"
                value={locality}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <CountrySelect
                label={globalMessages.recipient.country}
                name="country"
                onChange={({ value }) => {
                  if (value !== '') {
                    this.setState({
                      country: value,
                      hasEmptyCountry: false,
                    });
                  } else {
                    this.setState({
                      country: null,
                      hasEmptyCountry: true,
                    });
                  }
                }}
                error={hasEmptyCountry}
                required
              />
              <FormInput
                type="tel"
                label={globalMessages.recipient.phone}
                name="phone"
                value={phone}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                type="email"
                label={globalMessages.recipient.email}
                name="email"
                value={email}
                onChange={e => this.handleInputChange(e)}
              />
              <Button center>{globalMessages.create}</Button>
            </form>
          );
        }}
      </Mutation>
    );
  }
}

RecipientCreateForm.propTypes = {
  goToList: PropTypes.func.isRequired,
};

export default RecipientCreateForm;
