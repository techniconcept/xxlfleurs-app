/*
 * RecipientManagement Messages
 *
 * This contains all the text for the RecipientManagement container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.RecipientManagement';

export default defineMessages({
  headerList: {
    id: `${scope}.headerList`,
    defaultMessage: 'Mes destinataires',
  },
  headerEdit: {
    id: `${scope}.headerEdit`,
    defaultMessage: 'Edition du destinataire',
  },
  headerCreate: {
    id: `${scope}.headerCreate`,
    defaultMessage: 'Nouveau destinataire',
  },
  noRecipients: {
    id: `${scope}.noRecipients`,
    defaultMessage: "Vous n'avez aucun destinataire !",
  },
});
