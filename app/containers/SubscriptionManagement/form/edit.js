/**
 *
 * RecipientEditForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import Mutation from 'react-apollo/Mutation';
import Calendar from 'react-calendar';
import styled from 'styled-components';
import {
  Button,
  ActivityIndicator,
  FormInput,
  CountrySelect,
  RadioInput,
  Label,
} from '../../../components';
import globalMessages from '../../../global-message';
import {
  UPDATE_SUBSCRIPTION_RECIPIENT,
  GET_SUBSCRIPTION_RECIPIENT,
  GET_ALL_SUBSCRIPTION_RECIPIENTS,
} from '../../../queries';
import messages from '../messages';

const DivWithMarginBottom = styled.div`
  margin-bottom: 45px;
`;

const DivWithError = styled.div`
  color: red;
  margin-bottom: 10px;
`;

/* eslint-disable react/prefer-stateless-function */
class RecipientEditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasEmptyCountry: false,
      hasEmptyCalendar: true,
      calendarVisible: false,
      submitting: false,
    };
    this.tomorrow = new Date();
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);
    this.goToList = props.goToList;
  }

  handleInputChange(event) {
    const { target } = event;
    const { value, name } = target;

    this.setState({
      [name]: value,
    });
  }

  showField(label, showField, event, eventText) {
    if (event === label && showField) {
      return (
        <FormInput
          label={messages.eventTextLabel}
          name="eventText"
          value={eventText}
          onChange={e => this.handleInputChange(e)}
          required
        />
      );
    }
    return null;
  }

  showCalendar(label, showCalendar, event, shippingDate) {
    const { hasEmptyCalendar } = this.state;
    if (event === label && showCalendar) {
      const shippingRealDate = shippingDate
        ? new Date(shippingDate)
        : new Date(this.tomorrow);
      return (
        <DivWithMarginBottom>
          {hasEmptyCalendar && (
            <DivWithError className="error">
              {messages.dateErrorMsg}
            </DivWithError>
          )}
          <Calendar
            value={shippingRealDate}
            onChange={date => this.setState({ shippingDate: date })}
            minDate={this.tomorrow}
          />
        </DivWithMarginBottom>
      );
    }

    return null;
  }

  render() {
    const params = { id: this.props.id };
    return (
      <Mutation
        mutation={UPDATE_SUBSCRIPTION_RECIPIENT}
        onCompleted={() => {
          this.setState({
            submitting: false,
          });
          if (this.props.goToList) {
            this.props.goToList();
          }
        }}
        refetchQueries={[
          {
            query: GET_ALL_SUBSCRIPTION_RECIPIENTS,
            variables: {
              id: this.props.idSubscription,
            },
          },
        ]}
        awaitRefetchQueries
        ignoreResults
      >
        {updateRecipient => (
          <Query query={GET_SUBSCRIPTION_RECIPIENT} variables={params}>
            {({ loading, error, data }) => {
              if (loading) return <ActivityIndicator loading={loading} />;
              if (error) return <p>{error}</p>;
              const recipient = Object.assign(
                data.subscriptionRecipient.get,
                this.state,
              );

              const {
                name,
                address,
                postalCode,
                locality,
                country,
                phone,
                email,
                event,
                eventText,
                shippingDate,
              } = recipient;
              const { hasEmptyCountry, calendarVisible } = this.state;
              return (
                <div>
                  <form
                    onSubmit={e => {
                      e.preventDefault();
                      this.setState({
                        hasEmptyCountry: country === null,
                        hasEmptyCalendar:
                          calendarVisible && shippingDate === null,
                        submitting: true,
                      });
                      if (
                        !country ||
                        (calendarVisible && shippingDate === null)
                      ) {
                        this.setState({ submitting: false });
                        return;
                      }
                      updateRecipient({
                        variables: {
                          name,
                          address,
                          postalCode,
                          locality,
                          country,
                          phone,
                          email,
                          recipient: this.props.id,
                          event,
                          eventText,
                          shippingDate,
                        },
                      });
                    }}
                  >
                    <FormInput
                      label={globalMessages.recipient.name}
                      name="name"
                      value={name}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.address}
                      name="address"
                      value={address}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.postalCode}
                      name="postalCode"
                      value={postalCode}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      label={globalMessages.recipient.locality}
                      name="locality"
                      value={locality}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <CountrySelect
                      label={globalMessages.recipient.country}
                      name="country"
                      current={country}
                      onChange={({ value }) => {
                        if (value !== '') {
                          this.setState({
                            country: value,
                            hasEmptyCountry: false,
                          });
                        } else {
                          this.setState({
                            country: null,
                            hasEmptyCountry: true,
                          });
                        }
                      }}
                      required
                      error={hasEmptyCountry}
                    />
                    <FormInput
                      type="tel"
                      label={globalMessages.recipient.phone}
                      name="phone"
                      value={phone}
                      onChange={e => this.handleInputChange(e)}
                      required
                    />
                    <FormInput
                      type="email"
                      label={globalMessages.recipient.email}
                      name="email"
                      value={email}
                      onChange={e => this.handleInputChange(e)}
                    />
                    <div>
                      <Label label="Evénement"> </Label>
                      {data.message.allSubscriptionMessages.map(msg => (
                        <div key={msg.id}>
                          <RadioInput
                            key={msg.id}
                            id={msg.id}
                            name="selectedEvent"
                            label={msg.title}
                            checked={msg.title.trim() === event.trim()}
                            onClick={() => this.setState({ event: msg.title })}
                          />
                          {this.showField(
                            msg.title,
                            msg.showField,
                            event,
                            eventText,
                          )}
                          {this.showCalendar(
                            msg.title,
                            msg.showCalendar,
                            event,
                            shippingDate,
                          )}
                        </div>
                      ))}
                    </div>
                    {this.state.submitting && <ActivityIndicator loading />}
                    {!this.state.submitting && (
                      <Button center>{globalMessages.update}</Button>
                    )}
                  </form>
                </div>
              );
            }}
          </Query>
        )}
      </Mutation>
    );
  }
}

RecipientEditForm.propTypes = {
  id: PropTypes.string.isRequired,
  goToList: PropTypes.func.isRequired,
  idSubscription: PropTypes.string.isRequired,
};

export default RecipientEditForm;
