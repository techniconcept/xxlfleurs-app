/**
 *
 * RecipientCreateForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Mutation from 'react-apollo/Mutation';
import Calendar from 'react-calendar';
import styled from 'styled-components';
import { Query } from 'react-apollo';
import {
  FormInput,
  CountrySelect,
  Button,
  RadioInput,
  ActivityIndicator,
  Label,
} from '../../../components';
import globalMessages from '../../../global-message';
import {
  CREATE_SUBSCRIPTION_RECIPIENT,
  GET_ALL_SUBSCRIPTION_RECIPIENTS,
  GET_ALL_SUBSCRIPTION_MESSAGES,
} from '../../../queries';
import messages from '../messages';

const DivWithMarginBottom = styled.div`
  margin-bottom: 45px;
`;

const DivWithError = styled.div`
  color: red;
  margin-bottom: 10px;
`;

/* eslint-disable react/prefer-stateless-function */
class SubscriptionRecipientCreateForm extends React.Component {
  constructor(props) {
    super(props);
    this.goToList = props.goToList;
    this.tomorrow = new Date();
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);
    this.state = {
      hasEmptyCountry: true,
      hasEmptyCalendar: true,
      hasEmptyEvent: false,
      shippingDate: null,
      selectedEvent: null,
      calendarVisible: false,
      submitting: false,
      eventText: '',
      name: '',
      address: '',
      postalCode: '',
      locality: '',
      country: '',
      phone: '',
      email: '',
    };
  }

  handleInputChange(event) {
    const { target } = event;
    const { value, name } = target;

    this.setState({
      [name]: value,
    });
  }

  showField(label, showField) {
    if (this.state.selectedEvent === label && showField) {
      return (
        <FormInput
          label={messages.eventTextLabel}
          name="eventText"
          value={this.state.eventText}
          onChange={e => this.handleInputChange(e)}
          required
        />
      );
    }
    return null;
  }

  showCalendar(label, showCalendar) {
    const { hasEmptyCalendar } = this.state;
    if (this.state.selectedEvent === label && showCalendar) {
      return (
        <DivWithMarginBottom>
          {hasEmptyCalendar && (
            <DivWithError className="error">
              {messages.dateErrorMsg}
            </DivWithError>
          )}
          <Calendar
            value={this.state.shippingDate}
            onChange={date => {
              this.setState({ shippingDate: date, hasEmptyCalendar: false });
            }}
            minDate={this.tomorrow}
          />
        </DivWithMarginBottom>
      );
    }
    return null;
  }

  handleEventChange(label, showCalendar) {
    this.setState({
      selectedEvent: label,
      calendarVisible: showCalendar,
      hasEmptyEvent: false,
    });
  }

  render() {
    return (
      <Mutation
        mutation={CREATE_SUBSCRIPTION_RECIPIENT}
        onCompleted={() => {
          this.setState({
            submitting: false,
          });
          if (this.props.goToList) {
            this.props.goToList();
          }
        }}
        refetchQueries={[
          {
            query: GET_ALL_SUBSCRIPTION_RECIPIENTS,
            variables: {
              id: this.props.idSubscription,
            },
          },
        ]}
        awaitRefetchQueries
      >
        {createRecipient => {
          const {
            name,
            address,
            postalCode,
            locality,
            country,
            phone,
            email,
            hasEmptyCountry,
            selectedEvent,
            eventText,
            shippingDate,
            calendarVisible,
            hasEmptyEvent,
          } = this.state;
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                this.setState({
                  hasEmptyCountry: country === '',
                  hasEmptyCalendar: calendarVisible && shippingDate === null,
                  hasEmptyEvent: selectedEvent === null,
                  submitting: true,
                });
                if (
                  !country ||
                  (calendarVisible && shippingDate === null) ||
                  selectedEvent === null
                ) {
                  this.setState({
                    submitting: false,
                  });
                  window.scrollTo(0, 0);
                  return;
                }
                createRecipient({
                  variables: {
                    subscription: this.props.idSubscription,
                    name,
                    address,
                    postalCode,
                    locality,
                    country,
                    phone,
                    email,
                    event: selectedEvent,
                    eventText,
                    shippingDate,
                  },
                });
              }}
            >
              <FormInput
                label={globalMessages.recipient.name}
                name="name"
                value={name}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.address}
                name="address"
                value={address}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.postalCode}
                name="postalCode"
                value={postalCode}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                label={globalMessages.recipient.locality}
                name="locality"
                value={locality}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <CountrySelect
                label={globalMessages.recipient.country}
                name="country"
                onChange={({ value }) =>
                  this.setState({ country: value, hasEmptyCountry: false })
                }
                error={hasEmptyCountry}
              />
              <FormInput
                type="tel"
                label={globalMessages.recipient.phone}
                name="phone"
                value={phone}
                onChange={e => this.handleInputChange(e)}
                required
              />
              <FormInput
                type="email"
                label={globalMessages.recipient.email}
                name="email"
                value={email}
                onChange={e => this.handleInputChange(e)}
              />
              <div>
                <Label label="Evénement"> </Label>
                {hasEmptyEvent && (
                  <DivWithError>{messages.emptyEvent}</DivWithError>
                )}
                <Query query={GET_ALL_SUBSCRIPTION_MESSAGES}>
                  {({ loading, error, data }) => {
                    if (loading) return <ActivityIndicator loading={loading} />;
                    if (error) return <p>{error}</p>;
                    return data.message.allSubscriptionMessages.map(msg => (
                      <div key={msg.title}>
                        <RadioInput
                          key={msg.id}
                          id={msg.id}
                          name="selectedEvent"
                          label={msg.title}
                          checked={msg.title === this.state.selectedEvent}
                          onClick={() =>
                            this.handleEventChange(msg.title, msg.showCalendar)
                          }
                        />
                        {this.showField(msg.title, msg.showField)}
                        {this.showCalendar(msg.title, msg.showCalendar)}
                      </div>
                    ));
                  }}
                </Query>
              </div>
              {this.state.submitting && <ActivityIndicator loading />}
              {!this.state.submitting && (
                <Button center>{globalMessages.create}</Button>
              )}
            </form>
          );
        }}
      </Mutation>
    );
  }
}

SubscriptionRecipientCreateForm.propTypes = {
  goToList: PropTypes.func.isRequired,
  idSubscription: PropTypes.string.isRequired,
};

export default SubscriptionRecipientCreateForm;
