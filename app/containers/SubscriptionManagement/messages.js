/*
 * SubscriptionManagement Messages
 *
 * This contains all the text for the SubscriptionManagement container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.SubscriptionManagement';

export default defineMessages({
  header: 'Abonnements',
  headerWithCount: 'Abonnement n. ',
  introduction:
    'Souscrivez un abonnement annuel pour Fr. 350.- (dont 100.- pour la fondation) et faites livrer 4 bouquets de fleurs à qui vous voulez.',
  add: 'Ajouter un destinataire',
  addSubscription: 'Ajouter un abonnement',
  label: 'Destinataire',
  subDate: 'Votre abonnement a été activé le ',
  subRequired: 'Ajouter un destinataire pour activer votre abonnement',
  activateSub: 'Activer mon abonnement',
  eventTextLabel: "Nom de l'événement",
  dateErrorMsg: 'Veuillez choisir une date',
  deleteConfirmation:
    'Êtes-vous sûr(e) de vouloir supprimer ce destinataire ?\n' +
    "Le prix de l'abonnement reste inchangé",
  emptyEvent: 'Veuillez sélectionner un événement',
  errorPay:
    "Une erreur est survenue lors du paiement. Merci d'essayer plus tard.",
});
