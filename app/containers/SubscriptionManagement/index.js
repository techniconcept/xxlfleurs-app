/**
 *
 * SubscriptionManagement
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';
import styled from 'styled-components';
import Mutation from 'react-apollo/Mutation';
import { Query } from 'react-apollo';
import { Elements, StripeProvider } from 'react-stripe-elements';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import {
  Footer,
  Header,
  Icon,
  Menu,
  PageHeader,
  PageWrapper,
  Section,
} from '../../components';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';
import List from './context/List';
import Edit from './context/Edit';
import Create from './context/Create';
import SubscriptionsList from './context/SubscriptionsList';
import {
  CREATE_SUBSCRIPTION,
  DELETE_SUBSCRIPTION_RECIPIENT,
  GET_ALL_SUBSCRIPTION_RECIPIENTS,
  GET_ALL_SUBSCRIPTIONS,
  GET_PROFILE,
} from '../../queries';
import ActivityIndicator from '../../components/ActivityIndicator';
import { SUBSCRIPTION_RECIPIENT_NUMBER } from './constants';
import history from '../../utils/history';

const CONTEXT_LIST_SUBSCRIPTIONS = 'context_list_subscriptions';
const CONTEXT_LIST_RECIPIENTS = 'context_list_recipients';
const CONTEXT_EDIT_RECIPIENT = 'context_edit_recipient';
const CONTEXT_CREATE_RECIPIENT = 'context_create_recipient';

const CenterText = styled.div`
  margin-top: 1em;
  margin-bottom: 1em;
  padding: 0 15px;
  text-align: center;
`;

/* eslint-disable react/prefer-stateless-function */
export class SubscriptionManagement extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      context: CONTEXT_LIST_SUBSCRIPTIONS,
      idRecipient: null,
      idSubscription: null,
      recipientQuantity: 0,
      currentSubscriptionIndex: null,
      updateCard: false,
      submitting: false,
      showError: false,
    };
  }

  componentWillReceiveProps(props) {
    if (props.location.state && props.location.state.resetStep) {
      this.setState({ context: CONTEXT_LIST_SUBSCRIPTIONS });
    }
  }

  askForDelete(deleteRecipient, id) {
    if (
      /* eslint-disable-next-line no-alert */
      window.confirm(messages.deleteConfirmation)
    ) {
      const { recipientQuantity } = this.state;
      deleteRecipient({
        variables: {
          recipient: id,
        },
      });
      this.setState({ recipientQuantity: recipientQuantity - 1 });
    }
  }

  deleteButton() {
    const { idSubscription, idRecipient, recipientQuantity } = this.state;
    if (recipientQuantity > 1) {
      return (
        <Mutation
          mutation={DELETE_SUBSCRIPTION_RECIPIENT}
          onCompleted={() => this.goToSubscriptionRecipientList(idSubscription)}
          refetchQueries={[
            {
              query: GET_ALL_SUBSCRIPTION_RECIPIENTS,
              variables: {
                id: idSubscription,
              },
            },
          ]}
          awaitRefetchQueries
        >
          {deleteRecipient => (
            <Icon
              onClick={() => this.askForDelete(deleteRecipient, idRecipient)}
              right
              icon="trash"
              small
            />
          )}
        </Mutation>
      );
    }
    return null;
  }

  getNavigation() {
    const { context } = this.state;
    if (context === CONTEXT_LIST_RECIPIENTS) {
      return (
        <Icon
          onClick={() => {
            this.setState({
              idSubscription: null,
              currentSubscriptionIndex: null,
            });
            this.goToList();
          }}
          left
          icon="arrow-left"
        />
      );
    }
    if (context === CONTEXT_CREATE_RECIPIENT) {
      return (
        <Icon
          onClick={() =>
            this.goToSubscriptionRecipientList(this.state.idSubscription)
          }
          left
          icon="arrow-left"
        />
      );
    }
    if (context === CONTEXT_EDIT_RECIPIENT) {
      return (
        <div>
          <Icon
            onClick={() =>
              this.goToSubscriptionRecipientList(this.state.idSubscription)
            }
            left
            icon="arrow-left"
          />
          {this.deleteButton()}
        </div>
      );
    }

    return null;
  }

  getSection() {
    const { context, submitting, showError } = this.state;
    switch (context) {
      case CONTEXT_LIST_SUBSCRIPTIONS:
        return (
          <div>
            <Query query={GET_ALL_SUBSCRIPTIONS} fetchPolicy="network-only">
              {({ loading, error, data }) => {
                if (loading) return <ActivityIndicator loading={loading} />;
                if (error) return <p>{error}</p>;
                return (
                  <Mutation
                    mutation={CREATE_SUBSCRIPTION}
                    onCompleted={dataReceived => {
                      this.goToSubscriptionRecipientList(
                        dataReceived.subscription.create.id,
                      );
                    }}
                    awaitRefetchQueries
                  >
                    {createSubscription => (
                      <SubscriptionsList
                        onEditClick={(idSubscription, index) => {
                          this.goToSubscriptionRecipientList(idSubscription);
                          this.setState({ currentSubscriptionIndex: index });
                        }}
                        onCreateClick={() => {
                          createSubscription();
                        }}
                        data={data}
                      />
                    )}
                  </Mutation>
                );
              }}
            </Query>
          </div>
        );
      case CONTEXT_LIST_RECIPIENTS:
        return (
          <div>
            <Query
              query={GET_ALL_SUBSCRIPTION_RECIPIENTS}
              variables={{ id: this.state.idSubscription }}
              onCompleted={data => {
                this.setState({
                  recipientQuantity:
                    data.subscriptions.get.subscriptionRecipients.length,
                });
              }}
              fetchPolicy="network-only"
            >
              {({ loading, error, data }) => {
                if (loading) return <ActivityIndicator loading={loading} />;
                if (error) return <p>{error}</p>;
                return (
                  <StripeProvider apiKey={process.env.STRIPE_KEY}>
                    <Elements locale="fr">
                      <List
                        onEditClick={idRecipient =>
                          this.setState({
                            idRecipient,
                            context: CONTEXT_EDIT_RECIPIENT,
                          })
                        }
                        onCreateClick={() =>
                          this.setState({ context: CONTEXT_CREATE_RECIPIENT })
                        }
                        recipientLeft={
                          SUBSCRIPTION_RECIPIENT_NUMBER -
                          data.subscriptions.get.subscriptionRecipients.length
                        }
                        idSubscription={this.state.idSubscription}
                        data={data}
                        updateCard={this.state.updateCard}
                        onUpdateCard={() => {
                          const { updateCard } = this.state;
                          this.setState({ updateCard: !updateCard });
                        }}
                        onSubmit={(payOrder, card, stripe) => async () => {
                          await this.onActivateSubscriptionClicked(
                            payOrder,
                            card,
                            stripe,
                          );
                        }}
                        submitting={submitting}
                        onComplete={() =>
                          this.setState({ submitting: false, showError: false })
                        }
                        onError={() =>
                          this.setState({ submitting: false, showError: true })
                        }
                        showError={showError}
                      />
                    </Elements>
                  </StripeProvider>
                );
              }}
            </Query>
          </div>
        );
      case CONTEXT_EDIT_RECIPIENT:
        return (
          <Edit
            goToList={() => {
              this.goToSubscriptionRecipientList(this.state.idSubscription);
            }}
            idRecipient={this.state.idRecipient}
            idSubscription={this.state.idSubscription}
          >
            Edition
          </Edit>
        );
      case CONTEXT_CREATE_RECIPIENT:
        return (
          <Create
            goToList={() => {
              this.goToSubscriptionRecipientList(this.state.idSubscription);
            }}
            idSubscription={this.state.idSubscription}
          />
        );
      default:
        return '';
    }
  }

  async onActivateSubscriptionClicked(payOrder, card, stripe) {
    this.setState({ submitting: true, showError: false });
    if (!card) {
      const { token, error } = await stripe.createToken();
      if (error) {
        this.setState({ submitting: false });
      } else {
        payOrder({
          variables: {
            subscription: this.state.idSubscription,
            token: token.id,
          },
        });
      }
    } else {
      payOrder({
        variables: {
          subscription: this.state.idSubscription,
        },
      });
    }
  }

  goToList() {
    this.setState({ context: CONTEXT_LIST_SUBSCRIPTIONS });
  }

  goToSubscriptionRecipientList(idSubscription) {
    this.setState({
      idSubscription,
      context: CONTEXT_LIST_RECIPIENTS,
    });
  }

  renderlayout() {
    const { isAuthenticated } = this.props;
    const { currentSubscriptionIndex } = this.state;
    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            {this.getNavigation()}
            {currentSubscriptionIndex !== null && (
              <Header center>
                {messages.headerWithCount} {currentSubscriptionIndex}
              </Header>
            )}
            {currentSubscriptionIndex === null && (
              <Header center>{messages.header}</Header>
            )}
            <CenterText>{messages.introduction}</CenterText>
          </div>
        </PageHeader>

        <Section>{this.getSection()}</Section>

        <Footer />
      </PageWrapper>
    );
  }

  render() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated === false) return <Redirect to="/" />;

    return (
      <Query query={GET_PROFILE}>
        {({ loading, error, data }) => {
          if (loading) return <ActivityIndicator loading={loading} />;
          if (error) return <p>{error}</p>;
          if (
            !data.profile.current.name ||
            !data.profile.current.address ||
            !data.profile.current.locality ||
            !data.profile.current.phone ||
            !data.profile.current.postalCode ||
            !data.profile.current.country
          ) {
            history.push({
              pathname: '/compte/edition',
              state: { fromSubscription: true },
            });
          }
          return this.renderlayout();
        }}
      </Query>
    );
  }
}

SubscriptionManagement.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  location: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'subscriptionManagement', reducer });
const withSaga = injectSaga({ key: 'subscriptionManagement', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(SubscriptionManagement);
