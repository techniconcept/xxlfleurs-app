import { fromJS } from 'immutable';
import subscriptionManagementReducer from '../reducer';

describe('subscriptionManagementReducer', () => {
  it('returns the initial state', () => {
    expect(subscriptionManagementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
