import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the subscriptionManagement state domain
 */

const selectSubscriptionManagementDomain = state =>
  state.get('subscriptionManagement', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by SubscriptionManagement
 */

const makeSelectSubscriptionManagement = () =>
  createSelector(selectSubscriptionManagementDomain, substate =>
    substate.toJS(),
  );

export default makeSelectSubscriptionManagement;
export { selectSubscriptionManagementDomain };
