import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Mutation from 'react-apollo/Mutation';
import {
  GET_ALL_SUBSCRIPTION_RECIPIENTS,
  SET_SUBSCRIPTION_ACTIVE,
} from '../../../queries';
import BigButton from '../../../components/BigButton';
import messages from '../messages';
import CardForm from '../../../components/CardForm';
import Icon from '../../../components/Icon';

const DivWithMarginTop = styled.div`
  margin-top: 45px;
  text-align: center;
`;

const DivError = styled.div`
  margin-top: 25px;
  color: red;
`;

const Title = styled.div`
  font-size: 18px;
  line-height: 1.25;
  font-weight: 500;
  color: rgba(255, 112, 0, 1);
  margin-bottom: 5px;
`;

const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 5px;
`;

class List extends React.PureComponent {
  getLabelCustomer(name, event, shippingDate, allSubscriptionMessages) {
    let showDate = false;

    allSubscriptionMessages.map(msg => {
      if (msg.title === event && msg.showCalendar) {
        showDate = true;
        return true;
      }
      return false;
    });
    let label = `${name}: ${event}`;
    if (showDate && shippingDate) {
      label = `${label} (${new Date(shippingDate).toLocaleDateString()})`;
    }

    return label;
  }

  mapRecipient(data) {
    const { onEditClick } = this.props;
    return data.subscriptions.get.subscriptionRecipients.map(
      ({ id, name, event, shippingDate }) => (
        <BigButton
          key={id}
          headline={messages.label}
          text={this.getLabelCustomer(
            name,
            event,
            shippingDate,
            data.message.all,
          )}
          icon="user"
          onClick={() => onEditClick(id)}
        />
      ),
    );
  }

  displayButtonToCreate(number) {
    const { onCreateClick } = this.props;
    const buttons = [];

    for (let i = 0; i < number; i += 1) {
      buttons.push(
        <BigButton
          key={i}
          headline={messages.add}
          icon="user-plus"
          onClick={() => onCreateClick()}
        />,
      );
    }
    return buttons;
  }

  displayButtonToActivate(data) {
    const {
      onUpdateCard,
      updateCard,
      onSubmit,
      submitting,
      onComplete,
      onError,
      showError,
    } = this.props;
    if (
      data.subscriptions.get.subscriptionRecipients.length > 0 &&
      !data.subscriptions.get.subscriptionActive
    ) {
      return (
        <Mutation
          mutation={SET_SUBSCRIPTION_ACTIVE}
          refetchQueries={[
            {
              query: GET_ALL_SUBSCRIPTION_RECIPIENTS,
              variables: {
                id: this.props.idSubscription,
              },
            },
          ]}
          onCompleted={() => onComplete()}
          onError={() => onError()}
          awaitRefetchQueries
        >
          {activateSubscription => (
            <div>
              <TitleWrapper>
                <Title>Informations de paiement</Title>
                <Icon small icon="edit" color onClick={onUpdateCard} />
              </TitleWrapper>
              <CardForm
                submitting={submitting}
                updateCard={updateCard}
                onUpdateCard={onUpdateCard}
                onSubmit={onSubmit}
                payOrder={activateSubscription}
              />
              {showError && (
                <DivError className="error">{messages.errorPay}</DivError>
              )}
            </div>
          )}
        </Mutation>
      );
    }
    if (data.subscriptions.get.subscriptionDate) {
      return (
        <div>
          {messages.subDate}
          {new Date(
            data.subscriptions.get.subscriptionDate,
          ).toLocaleDateString()}
        </div>
      );
    }
    return <div>{messages.subRequired}</div>;
  }

  render() {
    const { data, recipientLeft } = this.props;
    return (
      <div>
        <div>{this.mapRecipient(data)}</div>
        <div>{this.displayButtonToCreate(recipientLeft)}</div>
        <DivWithMarginTop>
          {this.displayButtonToActivate(data)}
        </DivWithMarginTop>
      </div>
    );
  }
}

List.propTypes = {
  onCreateClick: PropTypes.func.isRequired,
  onEditClick: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  recipientLeft: PropTypes.number.isRequired,
  idSubscription: PropTypes.string.isRequired,
  updateCard: PropTypes.bool.isRequired,
  onUpdateCard: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  onComplete: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  showError: PropTypes.bool.isRequired,
};

export default List;
