import React from 'react';
import PropTypes from 'prop-types';

import RecipientEditForm from '../form/edit';

// eslint-disable-next-line func-names
const Edit = function(props) {
  return (
    <RecipientEditForm
      id={props.idRecipient}
      idSubscription={props.idSubscription}
      goToList={props.goToList}
    />
  );
};

Edit.propTypes = {
  goToList: PropTypes.func.isRequired,
  idRecipient: PropTypes.string.isRequired,
  idSubscription: PropTypes.string.isRequired,
};

export default Edit;
