import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import BigButton from '../../../components/BigButton';
import messages from '../messages';

const DivWithMarginTop = styled.div`
  margin-top: 45px;
  text-align: center;
`;

class SubscriptionsList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  mapSubscriptions(data) {
    const { onEditClick } = this.props;
    return data.subscriptions.all.map(({ id, creationDate }, index) => (
      <BigButton
        key={id}
        headline={`Abonnement ${index + 1}`}
        text={`Créé le ${new Date(creationDate).toLocaleDateString()}`}
        icon="subscription"
        onClick={() => onEditClick(id, index + 1)}
      />
    ));
  }

  render() {
    const { data } = this.props;
    return (
      <div>
        <div>{this.mapSubscriptions(data)}</div>
        <BigButton
          headline={messages.addSubscription}
          icon="add"
          onClick={() => this.props.onCreateClick()}
        />
        <DivWithMarginTop />
      </div>
    );
  }
}

SubscriptionsList.propTypes = {
  onCreateClick: PropTypes.func.isRequired,
  onEditClick: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

export default SubscriptionsList;
