import React from 'react';
import PropTypes from 'prop-types';

import SubscriptionRecipientCreateForm from '../form/create';

// eslint-disable-next-line func-names
const Create = function(props) {
  return (
    <SubscriptionRecipientCreateForm
      goToList={props.goToList}
      idSubscription={props.idSubscription}
    />
  );
};

Create.propTypes = {
  goToList: PropTypes.func.isRequired,
  idSubscription: PropTypes.string.isRequired,
};

export default Create;
