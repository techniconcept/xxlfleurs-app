/**
 *
 * Asynchronously loads the component for SubscriptionManagement
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
