/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';

import messages from './messages';
import {
  PageHeader,
  Header,
  PageWrapper,
  BigButton,
  Menu,
  Section,
  Footer,
} from '../../components';

import { signIn, signOut } from '../../utils/auth';

/* eslint-disable react/prefer-stateless-function */
export class Login extends React.PureComponent {
  render() {
    const { isAuthenticated } = this.props;

    const loginLabel = <FormattedMessage {...messages.login} />;
    const logoutLabel = <FormattedMessage {...messages.logout} />;

    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center big>
              <FormattedMessage {...messages.header} />
            </Header>
          </div>
        </PageHeader>
        <Section>
          {isAuthenticated === false ? (
            <BigButton
              headline={loginLabel}
              icon="sign-in-alt"
              onClick={() => signIn()}
            />
          ) : (
            <BigButton
              headline={logoutLabel}
              icon="sign-out-alt"
              onClick={() => signOut()}
            />
          )}
        </Section>
        <Footer />
      </PageWrapper>
    );
  }
}

Login.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createSelector(
  makeSelectIsAuthenticated(),
  isAuthenticated => ({
    isAuthenticated,
  }),
);

const mapDispatchToProps = dispatch => ({
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Login);
