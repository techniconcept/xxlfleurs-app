/*
 * CallbackAuth Messages
 *
 * This contains all the text for the CallbackAuth container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.CallbackAuth';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Chargement en cours...',
  },
});
