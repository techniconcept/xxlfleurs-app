/**
 *
 * CallbackAuth
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import { Redirect } from 'react-router';

import messages from './messages';
import {
  PageHeader,
  Header,
  Section,
  Footer,
  PageWrapper,
  ActivityIndicator,
  Logo,
} from '../../components';
import { handleAuthenticationCallback } from '../AuthProvider/actions';
import {
  makeSelectIsAuthenticated,
  makeSelectIsFetching,
  makeSelectLocation,
} from '../AuthProvider/selectors';

import { makeSelectIsRedirectAfterLogin } from '../NewOrder/selectors';
import { toStep } from '../NewOrder/actions';
import { STEP_CHOOSE_RECIPIENT } from '../NewOrder/constants';

const CallbackAuth = ({
  dispatch,
  location,
  isAuthenticated,
  isFetching,
  redirectToOrderAfterLogin,
}) => {
  if (/access_token|id_token|error/.test(location.hash) === false) {
    return <Redirect to="/" />;
  }

  if (isAuthenticated && redirectToOrderAfterLogin === true) {
    dispatch(toStep(STEP_CHOOSE_RECIPIENT));
    return <Redirect to="/commander" />;
  }

  if (isAuthenticated && redirectToOrderAfterLogin === false) {
    return <Redirect to="/compte" />;
  }

  if (isFetching === false) {
    dispatch(handleAuthenticationCallback());
  }

  return (
    <PageWrapper>
      <PageHeader>
        <div>
          <Logo compact />
          <Header center big>
            <FormattedMessage {...messages.header} />
          </Header>
        </div>
      </PageHeader>
      <Section>
        <ActivityIndicator loading={isFetching} />
      </Section>
      <Footer />
    </PageWrapper>
  );
};

CallbackAuth.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  redirectToOrderAfterLogin: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
  isAuthenticated: makeSelectIsAuthenticated(),
  redirectToOrderAfterLogin: makeSelectIsRedirectAfterLogin(),
  isFetching: makeSelectIsFetching(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(CallbackAuth);
