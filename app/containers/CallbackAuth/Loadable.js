/**
 *
 * Asynchronously loads the component for CallbackAuth
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
