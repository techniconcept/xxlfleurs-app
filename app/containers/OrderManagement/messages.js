/*
 * OrderManagement Messages
 *
 * This contains all the text for the OrderManagement container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.OrderManagement';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Mes commandes',
  },
  noOrder: {
    id: `${scope}.noOrder`,
    defaultMessage: "Vous n'avez encore pas effectué de commande !",
  },
});
