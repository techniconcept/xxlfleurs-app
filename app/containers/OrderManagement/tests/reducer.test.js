import { fromJS } from 'immutable';
import orderManagementReducer from '../reducer';

describe('orderManagementReducer', () => {
  it('returns the initial state', () => {
    expect(orderManagementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
