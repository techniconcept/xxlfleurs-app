/**
 *
 * OrderManagement
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';

import { Query } from 'react-apollo';
import Mutation from 'react-apollo/Mutation';
import messages from './messages';
import PageWrapper from '../../components/PageWrapper';
import PageHeader from '../../components/PageHeader';
import Header from '../../components/Header';
import Section from '../../components/Section';
import { CREATE_ORDER, GET_ALL_ORDERS } from '../../queries';
import Footer from '../../components/Footer';
import Order from '../../components/Order';
import Menu from '../../components/Menu';
import ActivityIndicator from '../../components/ActivityIndicator';
import Paragraph from '../../components/Paragraph';
import globalMessages from '../../global-message';
import BigButton from '../../components/BigButton';
import history from '../../utils/history';
import Icon from '../../components/Icon';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';
import { resetOrder, resumeOrder } from '../NewOrder/actions';
import Route from '../../route.config';

/* eslint-disable react/prefer-stateless-function */
export class OrderManagement extends React.PureComponent {
  redirectToNewOrder({ id, recipient, items }) {
    const { dispatch } = this.props;
    dispatch(resumeOrder(id, recipient, items[0]));
    history.push(Route.ORDER);
  }

  toNewOrder() {
    const { dispatch } = this.props;
    dispatch(resetOrder());
    history.push(Route.ORDER);
  }

  render() {
    const { isAuthenticated } = this.props;
    if (isAuthenticated === false) return <Redirect to="/" />;
    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Header center>
              <Icon icon="add" right onClick={() => this.toNewOrder()} />
              <FormattedMessage {...messages.header} />
            </Header>
          </div>
        </PageHeader>

        <Section>
          <Query query={GET_ALL_ORDERS}>
            {({ loading, error, data }) => {
              if (loading) return <ActivityIndicator loading={loading} />;
              if (error) return <p>{error}</p>;
              if (!data.order.all.length) {
                return (
                  <div>
                    <Paragraph center>
                      <FormattedMessage {...messages.noOrder} />
                    </Paragraph>
                    <BigButton
                      headline={globalMessages.newOrder}
                      text={globalMessages.everyWhere}
                      icon="truck"
                      onClick={() => this.toNewOrder()}
                    />
                  </div>
                );
              }

              data.order.all.sort((a, b) => {
                if (a.shippingDate > b.shippingDate) {
                  return 1;
                }
                if (b.shippingDate > a.shippingDate) {
                  return -1;
                }
                return 0;
              });
              return data.order.all.map(
                ({ id, recipient, shippingDate, items }) => (
                  <Mutation
                    key={id}
                    mutation={CREATE_ORDER}
                    onCompleted={response => {
                      const { order } = response;
                      this.redirectToNewOrder(order.create);
                    }}
                    // TODO:  Gestion des erreurs
                  >
                    {repeatOrder => (
                      <Order
                        recipient={recipient}
                        shippingDate={shippingDate}
                        items={items}
                        onRepeatClicked={() => {
                          repeatOrder({
                            variables: {
                              item: items[0].id,
                              recipient: recipient.id,
                            },
                          });
                        }}
                      />
                    )}
                  </Mutation>
                ),
              );
            }}
          </Query>
        </Section>

        <Footer />
      </PageWrapper>
    );
  }
}

OrderManagement.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(OrderManagement);
