/**
 *
 * Asynchronously loads the component for OrderManagement
 *
 */

import loadable from 'loadable-components';

export default loadable(() => import('./index'));
