/*
 * EditProfile Messages
 *
 * This contains all the text for the EditProfile container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.EditProfile';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Edition du compte',
  },
  name: 'Prénom, Nom',
  address: 'Adresse',
  locality: 'Localité',
  postalCode: 'NPA',
  phone: 'Téléphone',
  mobile: 'Mobile',
  country: 'Pays',
  email: 'Courrier électronique',
  buttonLabel: 'Modifier',
});
