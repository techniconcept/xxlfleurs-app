/**
 *
 * ProfileEditForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Mutation from 'react-apollo/Mutation';
import messages from '../messages';
import {
  Button,
  ActivityIndicator,
  FormInput,
  CountrySelect,
} from '../../../components';
import { UPDATE_PROFILE, GET_PROFILE } from '../../../queries';
import history from '../../../utils/history';
import globalMessages from '../../../global-message';

/* eslint-disable react/prefer-stateless-function */
class ProfileEditForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      address: props.address || '',
      postalCode: props.postalCode || '',
      locality: props.locality || '',
      country: props.country || '',
      phone: props.phone || '',
      mobile: props.mobile || '',
      email: props.email || '',
    };
  }

  render() {
    return (
      <Mutation
        mutation={UPDATE_PROFILE}
        update={(cache, { data: { updateProfile } }) => {
          cache.writeQuery({
            query: GET_PROFILE,
            data: { updateProfile },
          });
          history.push('/compte');
        }}
        onCompleted={() => {
          if (this.props.returnToOrder) {
            history.push('/commander');
          }
          if (this.props.returnToSubscription) {
            history.push('/subscription');
          }
        }}
      >
        {(updateProfile, { loading, error }) => (
          <div>
            {error && <p>Error :( Please try again</p>}
            {loading ? (
              <ActivityIndicator loading={loading} />
            ) : (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  const {
                    name,
                    address,
                    postalCode,
                    locality,
                    country,
                    phone,
                    mobile,
                    email,
                  } = this.state;
                  updateProfile({
                    variables: {
                      name,
                      address,
                      postalCode,
                      locality,
                      country,
                      phone,
                      mobile,
                      email,
                    },
                  });
                }}
              >
                <FormInput
                  label={globalMessages.name}
                  name="name"
                  value={this.state.name}
                  onChange={({ target }) => {
                    this.setState({ name: target.value });
                  }}
                  required
                />
                <FormInput
                  label={globalMessages.address}
                  name="address"
                  value={this.state.address}
                  onChange={({ target }) => {
                    this.setState({ address: target.value });
                  }}
                  required
                />
                <FormInput
                  label={globalMessages.postalCode}
                  name="postalCode"
                  value={this.state.postalCode}
                  onChange={({ target }) => {
                    this.setState({ postalCode: target.value });
                  }}
                  required
                />
                <FormInput
                  label={globalMessages.locality}
                  name="locality"
                  value={this.state.locality}
                  onChange={({ target }) => {
                    this.setState({ locality: target.value });
                  }}
                  required
                />
                <CountrySelect
                  label={globalMessages.country}
                  name="country"
                  current={this.state.country}
                  onChange={({ value }) => this.setState({ country: value })}
                  required
                />
                <FormInput
                  label={globalMessages.phone}
                  type="tel"
                  name="phone"
                  value={this.state.phone}
                  onChange={({ target }) => {
                    this.setState({ phone: target.value });
                  }}
                  required
                />
                <FormInput
                  label={globalMessages.email}
                  type="email"
                  name="email"
                  value={this.state.email}
                  onChange={({ target }) => {
                    this.setState({ email: target.value });
                  }}
                  required
                />
                <Button center>{messages.buttonLabel}</Button>
              </form>
            )}
          </div>
        )}
      </Mutation>
    );
  }
}

ProfileEditForm.propTypes = {
  name: PropTypes.string.isRequired,
  address: PropTypes.string,
  postalCode: PropTypes.string,
  locality: PropTypes.string,
  country: PropTypes.string,
  phone: PropTypes.string,
  mobile: PropTypes.string,
  email: PropTypes.string,
  returnToOrder: PropTypes.bool,
  returnToSubscription: PropTypes.bool,
};

export default ProfileEditForm;
