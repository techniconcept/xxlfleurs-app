/**
 *
 * EditProfile
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';
import { Query } from 'react-apollo';
import messages from './messages';
import { GET_PROFILE } from '../../queries';
import {
  PageWrapper,
  PageHeader,
  Menu,
  Header,
  Section,
  Footer,
  Icon,
  ActivityIndicator,
} from '../../components';
import ProfileEditForm from './form/edit';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';

/* eslint-disable react/prefer-stateless-function */
export class EditProfile extends React.PureComponent {
  render() {
    const { isAuthenticated, location } = this.props;
    if (isAuthenticated === false) return <Redirect to="/" />;

    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <div>
            <Icon icon="arrow-left" href="/compte" back />
            <Header center>
              <FormattedMessage {...messages.header} />
            </Header>
            {location.state && location.state.fromOrder ? (
              <Header center>
                {
                  "Merci de compléter votre compte avant d'effectuer une commande!"
                }
              </Header>
            ) : null}
            {location.state && location.state.fromSubscription ? (
              <Header center>
                {"Merci de compléter votre compte pour l'abonnement!"}
              </Header>
            ) : null}
          </div>
        </PageHeader>

        <Section>
          <Query query={GET_PROFILE}>
            {({ loading, error, data }) => {
              if (loading) return <ActivityIndicator loading={loading} />;
              if (error) return <p>{error}</p>;
              const { current } = data.profile;
              return (
                <ProfileEditForm
                  name={current.name}
                  address={current.address}
                  postalCode={current.postalCode}
                  locality={current.locality}
                  country={current.country}
                  email={current.email}
                  phone={current.phone}
                  mobile={current.mobile}
                  returnToOrder={location.state && location.state.fromOrder}
                  returnToSubscription={
                    location.state && location.state.fromSubscription
                  }
                />
              );
            }}
          </Query>
        </Section>

        <Footer />
      </PageWrapper>
    );
  }
}

EditProfile.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(EditProfile);
