import {
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  HANDLE_AUTHENTICATION_CALLBACK,
  HANDLE_CHECK_SESSION,
} from './constants';

export const handleAuthenticationCallback = () => ({
  type: HANDLE_AUTHENTICATION_CALLBACK,
});

export const handleCheckSession = () => ({
  type: HANDLE_CHECK_SESSION,
});

export const loginSuccess = user => ({
  type: LOGIN_SUCCESS,
  user,
});

export const loginError = e => ({
  type: LOGIN_ERROR,
  ...e,
});
