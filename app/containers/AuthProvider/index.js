import React from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectIsAuthenticated } from './selectors';
import saga from './saga';
import { handleCheckSession } from './actions';

import reducer from './reducer';

export class AuthProvider extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    const { isAuthenticated, dispatch } = this.props;
    if (isAuthenticated === false) {
      dispatch(handleCheckSession());
    }
    return <div>{React.Children.only(this.props.children)}</div>;
  }
}

AuthProvider.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withSaga = injectSaga({ key: 'auth', saga });

const withReducer = injectReducer({ key: 'auth', reducer });
const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AuthProvider);
