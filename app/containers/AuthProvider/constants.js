export const LOGIN_SUCCESS = 'XxlFleurs/authProvider/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'XxlFleurs/authProvider/LOGIN_ERROR';
export const HANDLE_CHECK_SESSION =
  'XxlFleurs/authProvider/HANDLE_CHECK_SESSION';
export const HANDLE_AUTHENTICATION_CALLBACK =
  'XxlFleurs/authProvider/HANDLE_AUTHENTICATION_CALLBACK';
