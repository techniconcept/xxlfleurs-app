import { createSelector } from 'reselect';

const selectApp = state => state.get('auth');
const selectRouter = state => state.get('router');

const makeSelectIsAuthenticated = () =>
  createSelector(selectApp, state => state.get('isAuthenticated'));

const makeSelectIsFetching = () =>
  createSelector(selectApp, state => state.get('isFetching'));

const makeSelectLocation = () =>
  createSelector(selectRouter, state => state.get('location').toJS());

export { makeSelectIsAuthenticated, makeSelectIsFetching, makeSelectLocation };
