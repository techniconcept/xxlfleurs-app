import { call, put, all, takeLatest } from 'redux-saga/effects';
import {
  HANDLE_AUTHENTICATION_CALLBACK,
  HANDLE_CHECK_SESSION,
} from './constants';
import { loginSuccess, loginError } from './actions';
import {
  handleAuthentication,
  checkSession,
  clearPersistedToken,
  persistToken,
} from '../../utils/auth';

export function* parseHash() {
  try {
    // Parse hahs
    const user = yield call(handleAuthentication);
    yield call(() => persistToken(user.idToken));

    // Check user configuration on the backend

    // Login success
    yield put(loginSuccess(user));
  } catch (e) {
    yield call(clearPersistedToken);
    yield put(loginError(e));
  }
}

export function* handleAuthenticationCallback() {
  yield takeLatest(HANDLE_AUTHENTICATION_CALLBACK, parseHash);
}

export function* checkCurrentSession() {
  try {
    const user = yield call(checkSession);
    yield call(() => persistToken(user.idToken));
    yield put(loginSuccess(user));
  } catch (e) {
    yield call(clearPersistedToken);
    yield put(loginError(e));
  }
}

export function* handleCheckSession() {
  yield takeLatest(HANDLE_CHECK_SESSION, checkCurrentSession);
}

export default function* appSaga() {
  yield all([handleAuthenticationCallback(), handleCheckSession()]);
}
