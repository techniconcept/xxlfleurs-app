import { fromJS } from 'immutable';
import {
  HANDLE_AUTHENTICATION_CALLBACK,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
} from './constants';

// Initial state
export const initialState = fromJS({
  isFetching: false,
  isAuthenticated: false,
  error: null,
  errorDescription: null,
});

function authProviderReducer(state = initialState, action) {
  switch (action.type) {
    case HANDLE_AUTHENTICATION_CALLBACK:
      return state.set('isFetching', true);
    case LOGIN_SUCCESS:
      return state
        .set('isFetching', false)
        .set('isAuthenticated', true)
        .set('name', action.user.name)
        .set('nickname', action.user.nickname)
        .set('email', action.user.email)
        .set('picture', action.user.picture)
        .set('expiresAt', action.user.expiresAt);
    case LOGIN_ERROR:
      return state
        .set('isFetching', false)
        .set('isAuthenticated', false)
        .set('error', action.error)
        .set('errorDescription', action.errorDescription);
    default:
      return state;
  }
}

export default authProviderReducer;

export { authProviderReducer };
