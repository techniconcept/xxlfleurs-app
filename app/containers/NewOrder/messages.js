/*
 * NewOrder Messages
 *
 * This contains all the text for the NewOrder container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NewOrder';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Sélectionnez un bouquet de fleurs',
  },
  headers: {
    step_select_item: 'Sélectionnez un bouquet de fleurs',
    step_login: 'Sélectionnez un bouquet de fleurs',
    step_choose_recipient: 'Destinataire',
    step_delivery_date: 'Date de livraison',
    step_pay: 'Résumé',
    step_finished: 'Merci pour votre commande',
  },
  customMessage: 'Votre message',
});
