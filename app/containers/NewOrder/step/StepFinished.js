import React from 'react';
import { Paragraph } from '../../../components';

// eslint-disable-next-line func-names
const StepFinished = function() {
  return (
    <Paragraph center>Un email de confirmation vous a été envoyé</Paragraph>
  );
};

StepFinished.propTypes = {};

export default StepFinished;
