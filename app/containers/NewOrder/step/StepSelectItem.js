import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import styled from 'styled-components';

import { GET_ALL_PRODUCTS } from '../../../queries';
import { ActivityIndicator, Product, Button } from '../../../components';
import boutquetImage from '../bouquet_300.png';

const Image = styled.img`
  display: block;
  width: 65%;
  margin: auto;
`;

// eslint-disable-next-line func-names
const StepSelectItem = function({ currentProduct, onSelectItem, onSubmit }) {
  return (
    <Query query={GET_ALL_PRODUCTS}>
      {({ loading, error, data }) => {
        if (loading) return <ActivityIndicator loading={loading} />;
        if (error) return <p>{error}</p>;
        return (
          <div>
            <Image
              src={boutquetImage}
              alt="Commandez un bouquet de fleur pour soutenir la fondation et faire plaisir."
            />
            {data.product.all.map(({ id, name, price, donation }) => (
              <Product
                key={id}
                id={id}
                current={currentProduct !== null && id === currentProduct.id}
                name={name}
                price={price}
                donation={donation}
                onClick={onSelectItem({
                  id,
                  name,
                  price,
                  donation,
                })}
              />
            ))}
            <Button
              center
              disabled={currentProduct === null}
              onClick={onSubmit}
            >
              Suivant
            </Button>
          </div>
        );
      }}
    </Query>
  );
};

StepSelectItem.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onSelectItem: PropTypes.func.isRequired,
  currentProduct: PropTypes.object,
};

export default StepSelectItem;
