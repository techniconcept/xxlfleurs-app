import React from 'react';
import PropTypes from 'prop-types';
import Calendar from 'react-calendar';
import Mutation from 'react-apollo/Mutation';
import { Query } from 'react-apollo';
import {
  Header,
  Button,
  RadioInput,
  ActivityIndicator,
  FormInput,
} from '../../../components';
import { UPDATE_ORDER, GET_ALL_MESSAGES } from '../../../queries';
import { DELIVERY_ASAP, DELIVERY_DATE } from '../constants';
import messages from '../messages';

// eslint-disable-next-line func-names
export class StepDeliveryDate extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onMessageClicked = props.onMessageClicked;
  }

  renderField(msg) {
    if (this.props.currentMessage === msg && msg.showField) {
      return (
        <div>
          <FormInput
            label={messages.customMessage}
            name="customMessage"
            value={this.props.customMessage}
            onChange={e => this.props.handleInputChange(e)}
            required
          />
        </div>
      );
    }
    return '';
  }

  render() {
    return (
      <Mutation
        mutation={UPDATE_ORDER}
        onCompleted={this.props.onMutationCompleted}
        onError={this.props.onMutationError}
      >
        {updateOrder => (
          <div>
            <RadioInput
              id="asap"
              name="dateChoice"
              label="Livraison sous 24 heures"
              checked={this.props.dateChoice === DELIVERY_ASAP}
              onClick={this.props.onDateOptionClicked(DELIVERY_ASAP)}
            />
            <RadioInput
              id="date"
              name="dateChoice"
              label="Définir une autre date"
              checked={this.props.dateChoice === DELIVERY_DATE}
              onClick={this.props.onDateOptionClicked(DELIVERY_DATE)}
            />
            {this.props.dateChoice === DELIVERY_DATE ? (
              <Calendar
                value={this.props.currentSelectedDate}
                onChange={date => this.props.onCalendarDayClicked(date)}
                minDate={this.props.minDate}
              />
            ) : null}
            <Header>Message</Header>
            <Query query={GET_ALL_MESSAGES}>
              {({ loading, error, data }) => {
                if (loading) return <ActivityIndicator loading={loading} />;
                if (error) return <p>{error}</p>;
                return data.message.all.map((msg, index) => {
                  if (!this.props.currentMessage && index === 0) {
                    // TODO Check first
                    // this.onMessageClicked(msg);
                  }
                  return (
                    <div key={msg.id}>
                      <RadioInput
                        key={msg.id}
                        id={msg.id}
                        name="message"
                        label={msg.title}
                        checked={msg === this.props.currentMessage}
                        onClick={() => this.props.onMessageClicked(msg)}
                      />
                      {this.renderField(msg)}
                    </div>
                  );
                });
              }}
            </Query>
            <Button
              center
              disabled={
                !(this.props.currentMessage && this.props.dateChoice) ||
                (this.props.currentMessage &&
                  this.props.currentMessage.showField &&
                  !this.props.customMessage)
              }
              onClick={this.props.onSubmit(updateOrder)}
            >
              Suivant
            </Button>
          </div>
        )}
      </Mutation>
    );
  }
}

StepDeliveryDate.propTypes = {
  currentSelectedDate: PropTypes.instanceOf(Date),
  minDate: PropTypes.instanceOf(Date),
  onMutationCompleted: PropTypes.func.isRequired,
  onMutationError: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  onDateOptionClicked: PropTypes.func.isRequired,
  onCalendarDayClicked: PropTypes.func.isRequired,
  onMessageClicked: PropTypes.func.isRequired,
  dateChoice: PropTypes.string.isRequired,
  currentMessage: PropTypes.object,
  customMessage: PropTypes.string,
  handleInputChange: PropTypes.func,
  showField: PropTypes.func,
};

export default StepDeliveryDate;
