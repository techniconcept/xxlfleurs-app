import React from 'react';

import { ActivityIndicator } from '../../../components';

// eslint-disable-next-line func-names
const StepLogin = function() {
  return (
    <div>
      <ActivityIndicator loading />
    </div>
  );
};

StepLogin.propTypes = {};

export default StepLogin;
