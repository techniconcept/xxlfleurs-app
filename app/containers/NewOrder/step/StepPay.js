import React from 'react';
import PropTypes from 'prop-types';
import Mutation from 'react-apollo/Mutation';
import styled from 'styled-components';
import { Recipient, ProductDetails } from '../../../components';
import { GET_ALL_ORDERS, PAY_ORDER } from '../../../queries';
import Icon from '../../../components/Icon';
import CardForm from '../../../components/CardForm';

const InfoWrapper = styled.div`
  padding: 15px;
  background: #fff;
  margin-bottom: 10px;
  border: 1px solid grey;
`;
const Title = styled.div`
  font-size: 18px;
  line-height: 1.25;
  font-weight: 500;
  color: rgba(255, 112, 0, 1);
  margin-bottom: 5px;
`;
const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 5px;
`;
const ErrorWrapper = styled.div`
  color: red;
  margin-top: 15px;
`;

/* eslint-disable no-shadow */
// eslint-disable-next-line func-names
const StepPay = function({
  onMutationCompleted,
  onMutationError,
  isLoading,
  item,
  recipient,
  shippingDate,
  message,
  onSubmit,
  updateCard,
  onUpdateCard,
}) {
  return (
    <Mutation
      mutation={PAY_ORDER}
      onCompleted={onMutationCompleted}
      refetchQueries={[{ query: GET_ALL_ORDERS }]}
      onError={onMutationError}
    >
      {(payOrder, { error }) => (
        <div>
          <Title>Bouquet sélectionné</Title>
          <InfoWrapper>
            <ProductDetails
              name={item.name}
              price={item.price}
              donation={item.donation}
            />
          </InfoWrapper>
          <Title>Destinataire</Title>
          <InfoWrapper>
            <Recipient
              name={recipient.name}
              address={recipient.address}
              postalCode={recipient.postalCode}
              locality={recipient.locality}
              phone={recipient.phone}
              country={recipient.country}
              context="details"
            />
          </InfoWrapper>
          <Title>Livraison prévue</Title>
          <InfoWrapper>{shippingDate.toLocaleDateString()}</InfoWrapper>
          <Title>Message</Title>
          <InfoWrapper>{message}</InfoWrapper>
          <TitleWrapper>
            <Title>Informations de paiement</Title>
            <Icon small icon="edit" color onClick={onUpdateCard} />
          </TitleWrapper>
          <CardForm
            submitting={isLoading}
            updateCard={updateCard}
            onUpdateCard={onUpdateCard}
            onSubmit={onSubmit}
            payOrder={payOrder}
          />
          <ErrorWrapper>
            {error && <span>{error.toString()}</span>}
          </ErrorWrapper>
        </div>
      )}
    </Mutation>
  );
};

StepPay.propTypes = {
  onMutationCompleted: PropTypes.func.isRequired,
  onMutationError: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  recipient: PropTypes.object.isRequired,
  shippingDate: PropTypes.instanceOf(Date).isRequired,
  message: PropTypes.string.isRequired,
  updateCard: PropTypes.bool.isRequired,
  onUpdateCard: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default StepPay;
