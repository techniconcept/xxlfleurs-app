import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import Mutation from 'react-apollo/Mutation';

import styled from 'styled-components';
import { GET_ALL_RECIPIENTS, CREATE_ORDER } from '../../../queries';
import { ActivityIndicator, Recipient, Button } from '../../../components';
import RecipientCreateForm from '../../RecipientManagement/form/create';

const RecipientWrapper = styled.div`
  padding: 15px;
  &:nth-child(even) {
    background: #fff;
  }
`;

// eslint-disable-next-line func-names
export class StepChooseRecipient extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showCreateNew: false,
    };
  }

  toggleMode() {
    const { showCreateNew } = this.state;
    this.setState({ showCreateNew: !showCreateNew });
  }

  render() {
    const {
      currentRecipient,
      onSubmit,
      onMutationCompleted,
      onMutationError,
    } = this.props;

    return (
      <Query query={GET_ALL_RECIPIENTS} partialRefetch>
        {({ loading, error, data }) => {
          if (loading) return <ActivityIndicator loading={loading} />;
          if (error) return <p>{error}</p>;

          if (data.recipient && !data.recipient.all.length) {
            return <RecipientCreateForm />;
          }

          const { showCreateNew } = this.state;

          return showCreateNew ? (
            <div>
              <Button large onClick={() => this.toggleMode()}>
                Choisir un destinataire existant
              </Button>
              <RecipientCreateForm goToList={() => this.toggleMode()} />
            </div>
          ) : (
            <div>
              <Button large onClick={() => this.toggleMode()}>
                Créer un nouveau destinataire
              </Button>
              <Mutation
                mutation={CREATE_ORDER}
                onCompleted={onMutationCompleted}
                onError={onMutationError}
              >
                {createOrder => (
                  <div>
                    {data.recipient.all.map(
                      ({
                        id,
                        name,
                        postalCode,
                        address,
                        locality,
                        phone,
                        country,
                      }) => (
                        <RecipientWrapper key={id}>
                          <Recipient
                            key={id}
                            name={name}
                            postalCode={postalCode}
                            address={address}
                            locality={locality}
                            phone={phone}
                            country={country}
                            context={
                              currentRecipient && currentRecipient.id === id
                                ? 'order-checked'
                                : 'order'
                            }
                            onClick={onSubmit(createOrder, id)}
                          />
                        </RecipientWrapper>
                      ),
                    )}
                  </div>
                )}
              </Mutation>
            </div>
          );
        }}
      </Query>
    );
  }
}

StepChooseRecipient.propTypes = {
  onMutationCompleted: PropTypes.func.isRequired,
  onMutationError: PropTypes.func,
  onSubmit: PropTypes.func.isRequired,
  currentRecipient: PropTypes.object,
};

export default StepChooseRecipient;
