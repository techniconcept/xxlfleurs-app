/**
 *
 * NewOrder
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Redirect } from 'react-router';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import PropTypes from 'prop-types';
import { StripeProvider, Elements } from 'react-stripe-elements';
import { Query } from 'react-apollo';
import { makeSelectIsAuthenticated } from '../AuthProvider/selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {
  PageWrapper,
  PageHeader,
  Header,
  Section,
  Footer,
  Menu,
  OrderBreadcrumb,
  Icon,
  ActivityIndicator,
} from '../../components';

import {
  StepSelectItem,
  StepLogin,
  StepChooseRecipient,
  StepDeliveryDate,
  StepPay,
  StepFinished,
} from './step/index';

import { signIn } from '../../utils/auth';

import {
  STEP_SELECT_ITEM,
  STEP_LOGIN,
  STEP_CHOOSE_RECIPIENT,
  STEP_DELIVERY_DATE,
  STEP_PAY,
  STEP_FINISHED,
  DELIVERY_ASAP,
} from './constants';

import makeSelectCurrentOrder, {
  makeSelectCurrentRecipient,
  makeSelectCurrentStep,
  makeSelectPreselectedProduct,
} from './selectors';
import {
  preselectProduct,
  setOrder,
  toStep,
  toStepBeforeLogin,
} from './actions';
import { GET_PROFILE } from '../../queries';
import history from '../../utils/history';

/* eslint-disable react/prefer-stateless-function */
export class NewOrder extends React.PureComponent {
  constructor(props) {
    super(props);

    this.tomorrow = new Date();
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);

    this.state = {
      isLoading: false,
      shippingDate: this.tomorrow,
      dateChoice: DELIVERY_ASAP,
      updateCard: false,
    };
  }

  getNavigation() {
    const { currentStep, navigateToStep } = this.props;
    switch (currentStep) {
      case STEP_SELECT_ITEM:
        return null;
      case STEP_LOGIN:
        return null;
      case STEP_CHOOSE_RECIPIENT:
        return (
          <Icon
            onClick={() => navigateToStep(STEP_SELECT_ITEM)}
            left
            icon="arrow-left"
          />
        );
      case STEP_DELIVERY_DATE:
        return (
          <Icon
            onClick={() => navigateToStep(STEP_CHOOSE_RECIPIENT)}
            left
            icon="arrow-left"
          />
        );
      case STEP_PAY:
        return (
          <Icon
            onClick={() => navigateToStep(STEP_DELIVERY_DATE)}
            left
            icon="arrow-left"
          />
        );
      case STEP_FINISHED:
        return null;
      default:
        return null;
    }
  }

  renderlayout() {
    const { isAuthenticated } = this.props;
    return (
      <PageWrapper>
        <PageHeader>
          <Menu isAuthenticated={isAuthenticated} />
          <Header center>
            {this.getNavigation()}
            {this.title()}
          </Header>
          {this.breadcrumb()}
        </PageHeader>
        <Section>{this.steps()}</Section>
        <Footer />
      </PageWrapper>
    );
  }

  render() {
    const { isAuthenticated } = this.props;
    return isAuthenticated ? (
      <Query query={GET_PROFILE}>
        {({ loading, error, data }) => {
          if (loading) return <ActivityIndicator loading={loading} />;
          if (error) return <p>{error}</p>;
          if (
            !data.profile.current.name ||
            !data.profile.current.address ||
            !data.profile.current.locality ||
            !data.profile.current.phone ||
            !data.profile.current.postalCode ||
            !data.profile.current.country
          ) {
            history.push({
              pathname: '/compte/edition',
              state: { fromOrder: true },
            });
          }
          return this.renderlayout();
        }}
      </Query>
    ) : (
      this.renderlayout()
    );
  }

  breadcrumb() {
    const { currentStep, currentProduct, currentRecipient } = this.props;
    switch (currentStep) {
      case STEP_CHOOSE_RECIPIENT:
      case STEP_DELIVERY_DATE:
        return (
          <OrderBreadcrumb item={currentProduct} recipient={currentRecipient} />
        );
      default:
        return null;
    }
  }

  title() {
    const { currentStep } = this.props;
    switch (currentStep) {
      case STEP_SELECT_ITEM:
        return messages.headers.step_select_item;
      case STEP_LOGIN:
        return messages.headers.step_login;
      case STEP_CHOOSE_RECIPIENT:
        return messages.headers.step_choose_recipient;
      case STEP_DELIVERY_DATE:
        return messages.headers.step_delivery_date;
      case STEP_PAY:
        return messages.headers.step_pay;
      case STEP_FINISHED:
        return messages.headers.step_finished;
      default:
        return '';
    }
  }

  get message() {
    if (this.state.message) {
      return this.state.message.showField
        ? this.state.customMessage
        : this.state.message.title;
    }
    return '';
  }

  stepSelectItem() {
    const {
      isAuthenticated,
      selectProduct,
      currentProduct,
      dispatch,
    } = this.props;
    return (
      <StepSelectItem
        currentProduct={currentProduct}
        onSelectItem={({ id, name, price, donation }) => () => {
          selectProduct({ id, name, price, donation });
        }}
        onSubmit={() => {
          if (isAuthenticated) {
            dispatch(toStep(STEP_CHOOSE_RECIPIENT));
          } else {
            dispatch(toStepBeforeLogin(STEP_LOGIN));
          }
        }}
      />
    );
  }

  stepLogin() {
    signIn();
    return <StepLogin />;
  }

  stepChooseRecipient() {
    const {
      dispatch,
      isAuthenticated,
      navigateToStep,
      currentOrder,
      currentProduct,
      currentRecipient,
    } = this.props;
    if (isAuthenticated === false) return <Redirect to="/" />;
    return (
      <StepChooseRecipient
        currentRecipient={currentRecipient}
        onMutationCompleted={({ order }) => {
          dispatch(
            setOrder(
              currentOrder || order.create.id,
              order.create.recipient,
              currentProduct,
            ),
          );
          navigateToStep(STEP_DELIVERY_DATE);
          this.setState({
            isLoading: true,
          });
        }}
        // TODO:  Gestion des erreurs
        onSubmit={(createOrder, id) => () => {
          createOrder({
            variables: {
              item: currentProduct.id,
              recipient: id,
            },
          });
        }}
      />
    );
  }

  stepDeliverDate() {
    const { isAuthenticated, navigateToStep, currentOrder } = this.props;
    if (isAuthenticated === false) return <Redirect to="/" />;
    return (
      <StepDeliveryDate
        onSubmit={updateOrder => () => {
          updateOrder({
            variables: {
              order: currentOrder,
              shippingDate: this.state.shippingDate.toISOString(),
              message: this.message,
            },
          });
        }}
        onMutationCompleted={() => {
          this.setState({
            isLoading: false,
          });
          navigateToStep(STEP_PAY);
        }}
        // TODO:  Gestion des erreurs
        loading={this.state.isLoading}
        currentSelectedDate={this.state.shippingDate}
        currentMessage={this.state.message}
        dateChoice={this.state.dateChoice}
        onCalendarDayClicked={shippingDate => this.setState({ shippingDate })}
        onMessageClicked={message => {
          this.setState({ message });
          if (this.state.dateChoice) {
            this.setState({
              isLoading: false,
            });
          }
        }}
        onDateOptionClicked={option => () => {
          this.setState({ dateChoice: option });
          if (this.state.message) {
            this.setState({
              isLoading: false,
            });
          }
        }}
        minDate={this.tomorrow}
        handleInputChange={event => {
          const { target } = event;
          const { value, name } = target;

          this.setState({
            [name]: value,
          });
        }}
        customMessage={this.state.customMessage}
      />
    );
  }

  stepPay() {
    const {
      isAuthenticated,
      navigateToStep,
      currentOrder,
      currentProduct,
      currentRecipient,
    } = this.props;
    const { isLoading } = this.state;
    if (isAuthenticated === false) return <Redirect to="/" />;
    return (
      <StripeProvider apiKey={process.env.STRIPE_KEY}>
        <Elements locale="fr">
          <StepPay
            isLoading={isLoading}
            item={currentProduct}
            recipient={currentRecipient}
            shippingDate={this.state.shippingDate}
            message={this.message}
            onSubmit={(payOrder, card, stripe) => async () => {
              this.setState({ isLoading: true });
              if (!card) {
                const { token, error } = await stripe.createToken();
                if (error) {
                  this.setState({ isLoading: false });
                } else {
                  payOrder({
                    variables: {
                      order: currentOrder,
                      token: token.id,
                    },
                  });
                }
              } else {
                payOrder({
                  variables: {
                    order: currentOrder,
                  },
                });
              }
            }}
            onMutationCompleted={() => navigateToStep(STEP_FINISHED)}
            updateCard={this.state.updateCard}
            onUpdateCard={() => {
              const { updateCard } = this.state;
              this.setState({ updateCard: !updateCard });
            }}
            onMutationError={() => {
              this.setState({ isLoading: false });
            }}
          />
        </Elements>
      </StripeProvider>
    );
  }

  stepFinished() {
    const { isAuthenticated } = this.props;
    if (isAuthenticated === false) return <Redirect to="/" />;
    return <StepFinished />;
  }

  steps() {
    const { currentStep, isAuthenticated } = this.props;
    switch (currentStep) {
      case STEP_SELECT_ITEM:
        return this.stepSelectItem();
      case STEP_LOGIN:
        return isAuthenticated ? this.stepChooseRecipient() : this.stepLogin();
      case STEP_CHOOSE_RECIPIENT:
        return this.stepChooseRecipient();
      case STEP_DELIVERY_DATE:
        return this.stepDeliverDate();
      case STEP_PAY:
        return this.stepPay();
      case STEP_FINISHED:
        return this.stepFinished();
      default:
        return <Redirect to="/" />;
    }
  }
}

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
  currentStep: makeSelectCurrentStep(),
  currentOrder: makeSelectCurrentOrder(),
  currentRecipient: makeSelectCurrentRecipient(),
  currentProduct: makeSelectPreselectedProduct(),
});

const mapDispatchToProps = dispatch => ({
  selectProduct: message => {
    dispatch(preselectProduct(message));
  },
  navigateToStep: step => {
    dispatch(toStep(step));
  },
  dispatch,
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'newOrder', reducer });
const withSaga = injectSaga({ key: 'newOrder', saga });

NewOrder.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  selectProduct: PropTypes.func.isRequired,
  navigateToStep: PropTypes.func.isRequired,
  currentStep: PropTypes.string.isRequired,
  currentOrder: PropTypes.string,
  currentRecipient: PropTypes.object,
  currentProduct: PropTypes.object,
};

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(NewOrder);
