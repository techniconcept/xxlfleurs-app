/*
 *
 * NewOrder constants
 *
 */

export const PRESELECT_PRODUCT = 'xxlfleur/NewOrder/SELECT_PRODUCT';
export const TO_STEP = 'xxlfleur/NewOrder/TO_STEP';
export const SET_ORDER = 'xxlfleur/NewOrder/SET_ORDER';
export const RESET_ORDER = 'xxlfleur/NewOrder/RESET_ORDER';

export const DELIVERY_ASAP = 'delivery.asap';
export const DELIVERY_DATE = 'delivery.date';

// Steps
export const STEP_SELECT_ITEM = 'step_select_item';
export const STEP_LOGIN = 'step_login';
export const STEP_CHOOSE_RECIPIENT = 'step_choose_recipient';
export const STEP_DELIVERY_DATE = 'step_delivery_date';
export const STEP_PAY = 'step_pay';
export const STEP_FINISHED = 'step_finished';
