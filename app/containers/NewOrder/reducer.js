/*
 *
 * NewOrder reducer
 *
 */

import { fromJS } from 'immutable';
import {
  PRESELECT_PRODUCT,
  RESET_ORDER,
  SET_ORDER,
  STEP_DELIVERY_DATE,
  STEP_SELECT_ITEM,
  TO_STEP,
} from './constants';

export const initialState = fromJS({
  preselectedProduct: null,
  currentRecipient: null,
  currentOrder: null,
  currentStep: STEP_SELECT_ITEM,
  mode: 'normal',
});

function newOrderReducer(state = initialState, action) {
  switch (action.type) {
    case PRESELECT_PRODUCT:
      return state.set('preselectedProduct', fromJS(action.id));
    case SET_ORDER:
      // eslint-disable-next-line no-case-declarations
      const localState = state
        .set('currentOrder', action.id)
        .set('currentRecipient', fromJS(action.recipient))
        .set('preselectedProduct', fromJS(action.product))
        .set('mode', action.mode);

      if (action.mode === 'resume') {
        return localState.set('currentStep', STEP_DELIVERY_DATE);
      }

      return localState;
    case RESET_ORDER:
      return state
        .set('currentStep', STEP_SELECT_ITEM)
        .set('currentOrder', null)
        .set('currentRecipient', null)
        .set('preselectedProduct', null);
    case TO_STEP:
      return state
        .set('currentStep', action.step)
        .set('mode', action.mode || state.get('mode'));
    default:
      return state;
  }
}

export default newOrderReducer;
