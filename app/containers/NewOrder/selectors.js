import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the newOrder state domain
 */

const selectNewOrderDomain = state => state.get('newOrder', initialState);

/**
 * Other specific selectors
 */

/**
 * Default selector used by NewOrder
 */

const makeSelectCurrentOrder = () =>
  createSelector(selectNewOrderDomain, substate =>
    substate.get('currentOrder'),
  );

const makeSelectCurrentStep = () =>
  createSelector(selectNewOrderDomain, substate => substate.get('currentStep'));

const makeSelectCurrentRecipient = () =>
  createSelector(
    selectNewOrderDomain,
    substate =>
      substate.get('currentRecipient')
        ? substate.get('currentRecipient').toJS()
        : null,
  );

const makeSelectPreselectedProduct = () =>
  createSelector(
    selectNewOrderDomain,
    substate =>
      substate.get('preselectedProduct')
        ? substate.get('preselectedProduct').toJS()
        : null,
  );

const makeSelectIsRedirectAfterLogin = () =>
  createSelector(
    selectNewOrderDomain,
    substate => substate.get('mode') === 'redirectAfterLogin',
  );

export default makeSelectCurrentOrder;
export {
  makeSelectCurrentOrder,
  makeSelectPreselectedProduct,
  makeSelectCurrentRecipient,
  makeSelectCurrentStep,
  makeSelectIsRedirectAfterLogin,
};
