/*
 *
 * NewOrder actions
 *
 */

import {
  TO_STEP,
  PRESELECT_PRODUCT,
  SET_ORDER,
  RESET_ORDER,
} from './constants';

export const preselectProduct = id => ({
  type: PRESELECT_PRODUCT,
  id,
});

export const toStep = step => ({
  type: TO_STEP,
  step,
});

export const toStepBeforeLogin = step => ({
  type: TO_STEP,
  mode: 'redirectAfterLogin',
  step,
});

export const setOrder = (id, recipient, product) => ({
  type: SET_ORDER,
  mode: 'normal',
  id,
  recipient,
  product,
});

export const resumeOrder = (id, recipient, product) => ({
  type: SET_ORDER,
  mode: 'resume',
  id,
  recipient,
  product,
});

export const resetOrder = () => ({
  type: RESET_ORDER,
});
