import { fromJS } from 'immutable';
import newOrderReducer from '../reducer';

describe('newOrderReducer', () => {
  it('returns the initial state', () => {
    expect(newOrderReducer(undefined, {})).toEqual(fromJS({}));
  });
});
