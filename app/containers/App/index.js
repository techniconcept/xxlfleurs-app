/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import NewOrder from 'containers/NewOrder/Loadable';
import RecipientManagement from 'containers/RecipientManagement/Loadable';
import OrderManagement from 'containers/OrderManagement/Loadable';
import ProfileManagement from 'containers/ProfileManagement/Loadable';
import SubscriptionManagement from 'containers/SubscriptionManagement/Loadable';
import EditProfile from 'containers/EditProfile/Loadable';
import Login from 'containers/Login/Loadable';
import Share from 'containers/Share/Loadable';
import CallbackAuth from 'containers/CallbackAuth/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Conditions from 'containers/Conditions/Loadable';
import GlobalStyle from '../../global-styles';
import { Header, Logo, PageWrapper, Paragraph } from '../../components';

const App = () => {
  if (process.env.MAINTENANCE_MODE) {
    return (
      <PageWrapper>
        <div>
          <Logo compact />
          <Header center big>
            {"L'application est en maintenance."}
          </Header>
          <Paragraph center teaser>
            {"Merci d'essayer à nouveau dans un moment."}
          </Paragraph>
        </div>
        <GlobalStyle />
      </PageWrapper>
    );
  }
  return (
    <div>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/commander" component={NewOrder} />
        <Route exact path="/destinataires" component={RecipientManagement} />
        <Route exact path="/commandes" component={OrderManagement} />
        <Route exact path="/compte" component={ProfileManagement} />
        <Route exact path="/compte/edition" component={EditProfile} />
        <Route exact path="/subscription" component={SubscriptionManagement} />
        <Route exact path="/share" component={Share} />
        <Route exact path="/connexion" component={Login} />
        <Route exact path="/callback" component={CallbackAuth} />
        <Route
          exact
          path="/politique-de-confidentialite"
          component={Conditions}
        />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </div>
  );
};

export default App;
