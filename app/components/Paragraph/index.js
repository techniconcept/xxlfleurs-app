/**
 *
 * Header
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H1 = styled.h1`
  display: block;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  line-height: 1.25;
  color: #656565;
  padding: 0 25px;
  margin: 25px 0;

  ${props =>
    props.center &&
    css`
      text-align: center;
    `};

  ${props =>
    props.teaser &&
    css`
      font-size: 18px;
      font-weight: 400;
    `};

  ${props =>
    props.color &&
    css`
      color: rgba(255, 112, 0, 1);
      padding: 0;
    `};
`;

function Paragraph(props) {
  return (
    <H1 center={props.center} teaser={props.teaser} color={props.color}>
      {props.children}
    </H1>
  );
}

Paragraph.propTypes = {
  children: PropTypes.any.isRequired,
  center: PropTypes.bool,
  teaser: PropTypes.bool,
  color: PropTypes.bool,
};

export default Paragraph;
