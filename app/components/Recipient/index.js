/**
 *
 * Recipient
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Modal from '../Modal';
import Icon from '../Icon';

const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;

const RecipientWrapper = styled.div`
  cursor: pointer;
  font-family: 'Ubuntu', sans-serif;
  font-size: 14px;
  line-height: 14px;
  display: flex;
  flex: 1;
  justify-content: space-between;
  transition: all 120ms ease-in;
`;

const Name = styled.div`
  font-size: 16px;
  line-height: 18px;
  font-weight: bold;
  margin-bottom: 5px;
`;

const Address = styled.div``;

const Phone = styled.div`
  margin-top: 10px;
`;

const Infos = styled.div`
  flex: 2;
`;

const IconsWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
`;

class Recipient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  render() {
    if (this.state.showModal) {
      return (
        <Modal
          onAcceptClick={() => {
            this.props.onDeleteClick();
          }}
          onDenyClick={() => this.setState({ showModal: false })}
        />
      );
    }
    return (
      <Row>
        <RecipientWrapper
          onClick={this.props.onClick}
          selected={this.props.context === 'order-checked'}
        >
          <Infos>
            <Name>{this.props.name}</Name>
            <Address>{this.props.address}</Address>
            <Address>
              {this.props.postalCode} {this.props.locality} (
              {this.props.country})
            </Address>
            <Phone>{this.props.phone}</Phone>
          </Infos>
        </RecipientWrapper>
        <RecipientIcons
          context={this.props.context}
          onClick={this.props.onClick}
          onAskForDelete={() => this.setState({ showModal: true })}
        />
      </Row>
    );
  }
}

Recipient.propTypes = {
  name: PropTypes.string.isRequired,
  address: PropTypes.string,
  postalCode: PropTypes.string,
  locality: PropTypes.string,
  phone: PropTypes.string,
  country: PropTypes.string,
  onClick: PropTypes.func,
  onDeleteClick: PropTypes.func,
  context: PropTypes.oneOf(['order', 'edition', 'details']).isRequired,
};

export default Recipient;

function RecipientIcons(props) {
  switch (props.context) {
    case 'order':
      return <Icon icon="arrow-right" onClick={props.onClick} color />;
    case 'order-checked':
      return <Icon icon="checked" onClick={props.onClick} color />;
    case 'edition':
      return (
        <IconsWrapper>
          <Icon icon="edit" onClick={props.onClick} small />
          <Icon icon="trash" onClick={props.onAskForDelete} small />
        </IconsWrapper>
      );
    case 'details':
      return null;
    default:
      return null;
  }
}

RecipientIcons.propTypes = {
  context: PropTypes.string,
  onClick: PropTypes.func,
  onAskForDelete: PropTypes.func,
};
