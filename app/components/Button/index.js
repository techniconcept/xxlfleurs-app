/**
 *
 * Button
 *
 */

import React from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

const ButtonElement = styled.button`
  cursor: pointer;
  background-color: rgba(255, 112, 0, 1);
  border: 0;
  color: white;
  text-transform: uppercase;
  text-align: center;
  font-size: 16px;
  line-height: 16px;
  font-family: 'Ubuntu', sans-serif;
  border-radius: 3px;
  padding: 0.6em 1.2em;
  margin-top: 1em;
  transition: all 120ms ease-in;

  &:hover {
  }

  &:disabled {
    opacity: 0.4;
    cursor: not-allowed;
  }

  ${props =>
    props.center &&
    css`
      margin: auto;
      display: block;
    `};

  ${props =>
    props.small &&
    css`
      text-transform: none;
      font-size: 14px;
      padding: 0.4em 1em;
    `};

  ${props =>
    props.margin &&
    css`
      margin-left: 10px;
      margin-right: 10px;
    `};

  ${props =>
    props.large &&
    css`
      width: 100%;
      margin-bottom: 20px;
    `};
`;

function Button(props) {
  return (
    <ButtonElement
      type="submit"
      center={props.center}
      disabled={props.disabled}
      small={props.small}
      margin={props.margin}
      onClick={props.onClick}
      large={props.large}
    >
      {props.children}
    </ButtonElement>
  );
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  center: PropTypes.bool,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  margin: PropTypes.bool,
  onClick: PropTypes.func,
  large: PropTypes.bool,
};

export default Button;
