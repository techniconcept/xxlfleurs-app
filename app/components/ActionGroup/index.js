/**
 *
 * ActionGroup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 4em;
`;

function ActionGroup(props) {
  return <Wrapper>{props.children}</Wrapper>;
}

ActionGroup.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ActionGroup;
