import React from 'react';
import PropTypes from 'prop-types';
import Input from '../Input';
import Label from '../Label';

const FormInput = ({ label, name, value, type, required, onChange }) => (
  <Label label={label}>
    <Input
      name={name}
      value={value}
      onChange={onChange}
      required={required}
      type={type || 'text'}
    />
  </Label>
);

FormInput.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool,
};

export default FormInput;
