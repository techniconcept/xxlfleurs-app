/**
 *
 * Input
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const InputElement = styled.input`
  display: block;
  border: 1px solid #cccccc;
  color: black;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  line-height: 16px;
  border-radius: 0;
  padding: 0.6em 0.6em;
  transition: all 120ms ease-in;
  width: 100%;
  margin-bottom: 5px;
  background-color: white;

  &:hover {
  }

  &:focus {
    border-color: rgba(255, 112, 0, 1);
  }
`;

function Input(props) {
  return (
    <InputElement
      type={props.type || 'text'}
      name={props.name}
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.onChange}
      required={props.required}
    />
  );
}

Input.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
};

export default Input;
