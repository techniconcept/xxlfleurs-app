/**
 *
 * PageHeader
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PageHeaderElement = styled.header`
  display: flex;
  flex-direction: column;
  margin-bottom: 1em;
  position: relative;
`;

function PageHeader(props) {
  return <PageHeaderElement>{props.children}</PageHeaderElement>;
}

PageHeader.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PageHeader;
