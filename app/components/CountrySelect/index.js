/**
 *
 * CountrySelect
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import Label from '../Label';
import countries from './country.json';

const buildOptions = c => {
  const options = [];
  Object.keys(c).forEach(k => {
    // eslint-disable-next-line no-prototype-builtins
    if (c.hasOwnProperty(k)) {
      options.push({ value: k, label: c[k] });
    }
  });
  return options;
};

const options = buildOptions(countries);
const customStyles = {
  option: provided => ({
    ...provided,
    color: 'black',
  }),
};
/* eslint-disable react/prefer-stateless-function */
class CountrySelect extends React.Component {
  state = {
    selectedOption: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: props.current,
    };
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    this.props.onChange(selectedOption);
  };

  render() {
    const { selectedOption } = this.state;
    const { label, name, error } = this.props;
    return (
      <Label label={label} error={error}>
        <Select
          name={name}
          value={options.filter(v => v.value === selectedOption).pop()}
          onChange={this.handleChange}
          options={options}
          placeholder={label}
          styles={customStyles}
        />
      </Label>
    );
  }
}

CountrySelect.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  current: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  error: PropTypes.bool,
};

export default CountrySelect;
