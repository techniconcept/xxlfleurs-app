/**
 *
 * PageWrapper
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 1em;
  max-width: 420px;
  margin-left: auto;
  margin-right: auto;
  min-height: 100vh;
`;

function PageWrapper(props) {
  return <Wrapper>{props.children}</Wrapper>;
}

PageWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export default PageWrapper;
