/*
 * OrderBreadcrumb Messages
 *
 * This contains all the text for the OrderBreadcrumb component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.OrderBreadcrumb';

export default defineMessages({});
