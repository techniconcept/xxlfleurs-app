/**
 *
 * OrderBreadcrumb
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BreadcrumbContainer = styled.div`
  font-size: 12px;
  padding: 5px 15px;
  margin: 15px;
  background-color: rgba(255, 112, 0, 1);
  color: white;
`;

/* eslint-disable react/prefer-stateless-function */
class OrderBreadcrumb extends React.PureComponent {
  render() {
    const { item, recipient } = this.props;
    if (!item) {
      return null;
    }
    return (
      <BreadcrumbContainer>
        {item ? `Bouquet ${item.name} > ` : ''}
        {recipient ? ` ${recipient.name}` : ''}
      </BreadcrumbContainer>
    );
  }
}

OrderBreadcrumb.propTypes = {
  item: PropTypes.object,
  recipient: PropTypes.object,
};

export default OrderBreadcrumb;
