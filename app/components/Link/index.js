/**
 *
 * Link
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const A = styled.a`
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  line-height: 16px;
  color: rgba(255, 112, 0, 1);

  &:hover {
    color: black;
  }
`;

function Link(props) {
  return <A href={props.href}>{props.children}</A>;
}

Link.propTypes = {
  children: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
};

export default Link;
