/*
 * SocialsShare Messages
 *
 * This contains all the text for the SocialsShare component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.SocialsShare';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the SocialsShare component!',
  },
});
