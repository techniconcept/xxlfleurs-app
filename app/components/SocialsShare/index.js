/**
 *
 * SocialsShare
 *
 */

import React from 'react';
import styled from 'styled-components';

import {
  WhatsappIcon,
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
} from 'react-share';

const url = 'https://fleurs.xxlfondation.ch/';
const description =
  "Aidez-nous à lutter contre la malnutrition et la famine en offrant des fleurs ; le bénéfice va entièrement à cette cause. \n\r Ouvrez et installez l'app mobile https://fleurs.xxlfondation.ch et offrez des fleurs en 5 clics !";

const SocialsWrapper = styled.section`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
const SocialItem = styled.section`
  margin-right: 10px;
`;
const A = styled.a``;

/* eslint-disable react/prefer-stateless-function */
class SocialsShare extends React.PureComponent {
  render() {
    return (
      <div>
        <SocialsWrapper>
          <SocialItem>
            <FacebookShareButton url={url} quote={description}>
              <FacebookIcon size={40} round />
            </FacebookShareButton>
          </SocialItem>
          <SocialItem>
            <A
              href={`https://api.whatsapp.com/send?text=${encodeURIComponent(
                description,
              )}`}
              target="_self"
            >
              <WhatsappIcon size={40} round />
            </A>
          </SocialItem>
          <SocialItem>
            <EmailShareButton url={url} subject={url} body={description}>
              <EmailIcon size={40} round />
            </EmailShareButton>
          </SocialItem>
          <SocialItem>
            <A
              href={`https://linkedin.com/shareArticle?url=${encodeURIComponent(
                url,
              )}&summary=${encodeURIComponent(description)}`}
              target="_self"
            >
              <LinkedinIcon size={40} round />
            </A>
          </SocialItem>
        </SocialsWrapper>
      </div>
    );
  }
}

SocialsShare.propTypes = {};

export default SocialsShare;
