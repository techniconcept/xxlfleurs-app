/*
 * CheckoutForm Messages
 *
 * This contains all the text for the CheckoutForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.CheckoutForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CheckoutForm component!',
  },
});
