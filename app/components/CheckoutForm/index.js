/**
 *
 * CheckoutForm
 *
 */

import React from 'react';
import { CardElement } from 'react-stripe-elements';

/* eslint-disable react/prefer-stateless-function */
class CheckoutForm extends React.PureComponent {
  render() {
    return (
      <div>
        <CardElement
          style={{
            base: {
              marginTop: 15,
            },
          }}
        />
      </div>
    );
  }
}

CheckoutForm.propTypes = {};

export default CheckoutForm;
