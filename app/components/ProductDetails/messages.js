/*
 * ProductDetails Messages
 *
 * This contains all the text for the ProductDetails component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ProductDetails';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ProductDetails component!',
  },
});
