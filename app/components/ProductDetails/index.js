/**
 *
 * ProductDetails
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Name = styled.div`
  font-weight: bold;
  margin-bottom: 5px;
`;
const Price = styled.div`
  color: rgba(255, 112, 0, 1);
  margin-bottom: 5px;
`;

function ProductDetails(props) {
  return (
    <div>
      <Name>{props.name}</Name>
      <Price>CHF {props.price}</Price>
      <div>CHF {props.donation} pour la fondation</div>
    </div>
  );
}

ProductDetails.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  donation: PropTypes.number.isRequired,
};

export default ProductDetails;
