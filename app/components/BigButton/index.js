/**
 *
 * Button
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import history from '../../utils/history';
import { ChooseIcon } from '../../utils/icons';

const ButtonElement = styled.button`
  cursor: pointer;
  display: flex;
  align-items: center;
  color: rgba(255, 112, 0, 1);
  background-color: white;

  border: 1px solid rgba(255, 112, 0, 1);

  text-transform: uppercase;
  text-align: left;
  font-size: 16px;
  line-height: 16px;
  font-family: 'Ubuntu', sans-serif;
  border-radius: 3px;
  padding: 0.9em 1.2em;
  margin-bottom: 1em;
  transition: all 120ms ease-in;
  width: 100%;
  min-height: 80px;

  &:hover {
    background-color: rgba(255, 112, 0, 1);
    color: white;
  }

  ${props =>
    props.disabled &&
    css`
      color: rgba(255, 112, 0, 0.5);
      border: 1px solid rgba(255, 112, 0, 0.5);
      &:hover {
        color: rgba(255, 112, 0, 0.5);
        background-color: white;
        cursor: default;
      }
    `};
`;

const Media = styled.div`
  width: 2.5em;
  margin-right: 0.75em;
`;

const Content = styled.div`
  flex: 1;
  text-align: right;
`;

const Headline = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 0.2em;
`;

const Text = styled.div`
  font-size: 16px;
  text-transform: none;
`;

// eslint-disable-next-line func-names
const BigButton = function(props) {
  const { onClick, href, disabled } = props;
  return (
    <ButtonElement
      type="button"
      onClick={() => {
        // eslint-disable-next-line no-unused-expressions
        onClick ? onClick() : history.push(href || '#');
      }}
      disabled={disabled}
    >
      <Media>{ChooseIcon(props.icon)}</Media>
      <Content>
        <Headline>{props.headline}</Headline>
        <Text>{props.text}</Text>
      </Content>
    </ButtonElement>
  );
};

BigButton.propTypes = {
  headline: PropTypes.any.isRequired,
  text: PropTypes.any,
  icon: PropTypes.oneOf([
    'truck',
    'history',
    'plane',
    'users',
    'user',
    'sign-in-alt',
    'sign-out-alt',
    'subscription',
    'add',
    'start',
    'user-plus',
  ]),
  href: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export default BigButton;
