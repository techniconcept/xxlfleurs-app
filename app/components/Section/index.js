/**
 *
 * Section
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SectionElement = styled.section`
  margin-top: 2em;
  margin-bottom: 2em;
  padding: 0 15px;
`;

function Section(props) {
  return <SectionElement>{props.children}</SectionElement>;
}

Section.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Section;
