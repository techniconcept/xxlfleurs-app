/**
 *
 * CardForm
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import styled from 'styled-components';
import { injectStripe } from 'react-stripe-elements';
import ActivityIndicator from '../ActivityIndicator';
import { GET_CARD } from '../../queries';
import { Button, CheckoutForm } from '../index';

const InfoWrapper = styled.div`
  padding: 15px;
  background: #fff;
  margin-bottom: 10px;
  border: 1px solid grey;
`;

function CardForm(props) {
  const { submitting, updateCard, onSubmit, payOrder, stripe } = props;
  return (
    <Query query={GET_CARD} fetchPolicy="network-only">
      {({ loading, error, data }) => {
        if (loading) return <ActivityIndicator loading={loading} />;
        if (error) return <p>{error}</p>;
        const form = <CheckoutForm />;
        const { card } = data.profile;
        if (!updateCard && card) {
          return (
            <div>
              <InfoWrapper>
                <div
                  style={{
                    marginBottom: 15,
                  }}
                >
                  **** **** **** **** {card}
                </div>
              </InfoWrapper>
              <Button
                disabled={submitting === true}
                center
                onClick={onSubmit(payOrder, !updateCard && card, stripe)}
              >
                Payer
              </Button>
            </div>
          );
        }
        return (
          <div>
            <InfoWrapper>{form}</InfoWrapper>
            <Button
              disabled={submitting === true}
              center
              onClick={onSubmit(payOrder, undefined, stripe)}
            >
              Payer
            </Button>
          </div>
        );
      }}
    </Query>
  );
}

CardForm.propTypes = {
  submitting: PropTypes.bool.isRequired,
  updateCard: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  payOrder: PropTypes.func.isRequired,
  stripe: PropTypes.object.isRequired,
};

export default injectStripe(CardForm);
