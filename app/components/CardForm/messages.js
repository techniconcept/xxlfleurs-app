/*
 * CardForm Messages
 *
 * This contains all the text for the CardForm component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.CardForm';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CardForm component!',
  },
});
