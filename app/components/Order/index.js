/**
 *
 * Order
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from '../Icon';

const OrderDiv = styled.div`
  font-family: 'Ubuntu', sans-serif;
  font-size: 14px;
  line-height: 14px;
  padding: 15px;
  display: flex;
  &:nth-child(even) {
    background: #fff;
  }
`;
const Name = styled.div`
  font-size: 16px;
  line-height: 18px;
  font-weight: bold;
  margin: 5px 0;
`;
const Div = styled.div`
  flex: 1;
`;

const Action = styled.div``;

function Order(props) {
  return (
    <OrderDiv>
      <Div>
        <Div>{new Date(props.shippingDate).toLocaleDateString()}</Div>
        <Name>{props.items[0].name}</Name>
        <Div>{props.recipient.name}</Div>
      </Div>
      {props.recipient.status === 'open' ? (
        <Action>
          <Icon icon="redo" onClick={props.onRepeatClicked} />
        </Action>
      ) : null}
    </OrderDiv>
  );
}

Order.propTypes = {
  shippingDate: PropTypes.string.isRequired,
  recipient: PropTypes.object.isRequired,
  items: PropTypes.array.isRequired,
  onRepeatClicked: PropTypes.func.isRequired,
};

export default Order;
