/**
 *
 * Footer
 *
 */

import React from 'react';
import styled from 'styled-components';
import SocialsShare from '../SocialsShare';
import Logo from '../Logo';

const FooterElement = styled.footer`
  display: flex;
  margin-top: 3em;
  padding: 1em;
  align-items: flex-start;
  justify-content: space-between;
  border-top: 1px solid rgba(255, 112, 0, 1);
  position: relative;
`;

function Footer() {
  return (
    <FooterElement>
      <Logo small />
      <SocialsShare />
    </FooterElement>
  );
}

Footer.propTypes = {};

export default Footer;
