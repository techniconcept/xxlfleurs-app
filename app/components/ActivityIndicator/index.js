/**
 *
 * ActivityIndicator
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { ClipLoader } from 'react-spinners';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

const Div = styled.div`
  display: flex;
`;

/* eslint-disable react/prefer-stateless-function */
function ActivityIndicator(props) {
  return (
    <Div>
      <ClipLoader
        css={{ override }}
        sizeUnit="px"
        size={45}
        color="black"
        loading={props.loading}
      />
    </Div>
  );
}

ActivityIndicator.propTypes = {
  loading: PropTypes.bool.isRequired,
};

export default ActivityIndicator;
