/*
 * ActivityIndicator Messages
 *
 * This contains all the text for the ActivityIndicator component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.ActivityIndicator';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ActivityIndicator component!',
  },
});
