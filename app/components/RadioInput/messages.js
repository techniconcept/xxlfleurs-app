/*
 * RadioInput Messages
 *
 * This contains all the text for the RadioInput component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RadioInput';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RadioInput component!',
  },
});
