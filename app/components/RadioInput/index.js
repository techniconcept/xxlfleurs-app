/**
 *
 * RadioInput
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import Icon from '../Icon';

const ChoiceInput = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  &:checked {
    + label {
      border-color: rgba(255, 112, 0, 1);
      background-color: rgba(255, 112, 0, 1);
      color: white;
    }
  }
`;

const ChoiceLabel = styled.label`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: bold;
  font-family: 'Ubuntu', sans-serif;
  font-size: 14px;
  line-height: 14px;
  padding: 15px;
  display: flex;
  border: 1px solid gray;
  border-radius: 3px;
  margin-bottom: 1em;
  cursor: pointer;
  transition: all 120ms ease-in;
  &:hover {
    border-color: rgba(255, 112, 0, 1);
    background-color: rgba(255, 112, 0, 1);
    color: white;

    > button {
      color: white !important;
    }
  }
`;

const IconWrapper = styled.div`
  margin-top: 2px;
  height: 32px;
  overflow: hidden;

  > button {
    color: transparent;
  }

  > button > div {
    width: 1.8em;
  }

  svg {
    fill: white;
    height: 100%;
  }

  ${props =>
    props.checked &&
    css`
      > button {
        color: white;
      }
    `};
`;

function RadioInput({ id, name, label, onClick, checked }) {
  return (
    <div>
      <ChoiceInput
        id={id}
        type="radio"
        name={name}
        onChange={onClick}
        checked={checked}
      />
      <ChoiceLabel htmlFor={id}>
        <IconWrapper checked={checked}>
          {checked ? <Icon icon="checked" /> : <Icon icon="unchecked" />}
        </IconWrapper>
        {label}
      </ChoiceLabel>
    </div>
  );
}

RadioInput.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  checked: PropTypes.bool,
};

export default RadioInput;
