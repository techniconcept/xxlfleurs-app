/**
 *
 * Header
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const H1 = styled.h1`
  display: block;
  font-family: 'Ubuntu', sans-serif;
  font-size: 18px;
  line-height: 1.25;
  font-weight: 500;
  color: rgba(255, 112, 0, 1);
  margin-bottom: 0.5em;
  margin-top: 1em;

  ${props =>
    props.center &&
    css`
      text-align: center;
    `};

  font-size: 18px;
  ${props =>
    props.big &&
    css`
      color: #656565;
      text-transform: uppercase;
      font-size: 28px;
    `};
`;

const InfoEnv = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  color: rgba(255, 112, 0, 1);
  font-size: 0.6em;
`;

function Header(props) {
  return (
    <div>
      {process.env.SERVER_ENV === 'preproduction' && <InfoEnv>DEV</InfoEnv>}
      {process.env.SERVER_ENV === 'local' && <InfoEnv>LOCAL</InfoEnv>}
      <H1 center={props.center} big={props.big}>
        {props.children}
      </H1>
    </div>
  );
}

Header.propTypes = {
  children: PropTypes.any.isRequired,
  center: PropTypes.bool,
  big: PropTypes.bool,
};

export default Header;
