/**
 *
 * Menu
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MenuItem from '../MenuItem';
import messages from './messages';

const MenuContainer = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  padding-bottom: 10px;
  padding-right: 10px;
  margin-bottom: 10px;
  border-bottom: 1px solid rgba(255, 112, 0, 1);
`;

const RightSideToolbar = styled.div`
  display: flex;
  margin-left: auto;

  > * {
    white-space: nowrap;
    margin-right: 1em;

    &:last-child {
      margin-right: 0;
    }
  }
`;

/* eslint-disable react/prefer-stateless-function */
class Menu extends React.Component {
  render() {
    const { isAuthenticated } = this.props;
    return (
      <MenuContainer>
        <MenuItem icon="xxl" href="/" weak />
        <RightSideToolbar>
          {isAuthenticated ? (
            <MenuItem
              icon="truck"
              label={messages.items.orders}
              href="/commandes"
              color
            />
          ) : null}
          {isAuthenticated ? (
            <MenuItem
              icon="subscription"
              label={messages.items.subscription}
              href="/subscription"
              color
              resetStep
            />
          ) : null}
          {isAuthenticated ? (
            <MenuItem
              icon="user"
              label={messages.items.user}
              href="/compte"
              color
            />
          ) : null}
          {isAuthenticated === false ? (
            <MenuItem
              icon="user"
              label={messages.items.login}
              href="/connexion"
              color
            />
          ) : null}
        </RightSideToolbar>
      </MenuContainer>
    );
  }
}

Menu.defaultProps = {
  isAuthenticated: false,
};

Menu.propTypes = {
  isAuthenticated: PropTypes.bool,
};

export default Menu;
