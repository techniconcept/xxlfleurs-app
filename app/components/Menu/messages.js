/*
 * Menu Messages
 *
 * This contains all the text for the Menu component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  items: {
    orders: 'Livraisons',
    recipients: 'Destinataires',
    subscription: 'Abonnement',
    user: 'Mon compte',
    login: 'Connexion',
  },
});
