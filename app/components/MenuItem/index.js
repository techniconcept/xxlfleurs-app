/**
 *
 * MenuItem
 *
 */

import React from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';
import Icon from '../Icon';

const MenuItemContainer = styled.div`
  flex: 1;
  cursor: pointer;
  font-size: 12px;
  display: flex;
  button {
    margin: auto;
    min-height: 45px;
    height: 100%;
    padding: 0;
    div {
      margin-right: 0;
    }
  }
  ${props =>
    props.weak &&
    css`
      flex: 0;
      margin-right: 2em;
      padding-left: 30px;
    `};
`;

/* eslint-disable react/prefer-stateless-function */
function MenuItem(props) {
  return (
    <MenuItemContainer weak={props.weak}>
      <Icon
        icon={props.icon}
        label={props.label}
        href={props.href}
        color={props.color}
        resetStep={props.resetStep}
      />
    </MenuItemContainer>
  );
}

MenuItem.propTypes = {
  icon: PropTypes.string,
  label: PropTypes.string,
  href: PropTypes.string,
  weak: PropTypes.bool,
  color: PropTypes.bool,
  resetStep: PropTypes.bool,
};

export default MenuItem;
