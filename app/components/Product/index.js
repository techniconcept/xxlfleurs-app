/**
 *
 * Order
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ProductInput = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
  &:checked {
    + label {
      border-color: rgba(255, 112, 0, 1);
      background-color: rgba(255, 112, 0, 1);
      color: white;
    }
  }
`;

const ProductLabel = styled.label`
  font-family: 'Ubuntu', sans-serif;
  font-size: 14px;
  line-height: 14px;
  padding: 15px;
  display: flex;
  justify-content: space-between;
  border: 1px solid gray;
  border-radius: 3px;
  margin-bottom: 1em;
  cursor: pointer;
  transition: all 120ms ease-in;
  &:hover {
    border-color: rgba(255, 112, 0, 1);
    background-color: rgba(255, 112, 0, 1);
    color: white;
  }
`;

const InnerLabel = styled.span`
  display: inline-block;
`;

function Product({ id, current, name, onClick, price, donation }) {
  return (
    <div>
      <ProductInput
        id={id}
        type="radio"
        name="product"
        defaultChecked={current}
      />
      <ProductLabel id={name} htmlFor={id} onClick={onClick}>
        <InnerLabel>{name}</InnerLabel>
        <InnerLabel>
          {price}
          .- dont {donation}
          .- pour la fondation
        </InnerLabel>
      </ProductLabel>
    </div>
  );
}

Product.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  current: PropTypes.bool.isRequired,
  price: PropTypes.number.isRequired,
  donation: PropTypes.number.isRequired,
  onClick: PropTypes.func,
};

export default Product;
