/**
 *
 * UserInfo
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from '../Icon';

const UserInfos = styled.div`
  font-size: 14px;
  line-height: 16px;
  display: flex;
  padding-bottom: 15px;
`;

const Infos = styled.div`
  flex: 1;
`;
const NameContainer = styled.div`
  font-weight: bold;
`;
const Container = styled.div`
  margin: 10px 0;
`;
const Actions = styled.div``;

function UserInfo(props) {
  return (
    <UserInfos>
      <Infos>
        <NameContainer>{props.name}</NameContainer>
        <Container>
          <div>{props.address}</div>
          <div>
            {props.postalCode} {props.locality}
          </div>
          <div>{props.country}</div>
        </Container>
        <Container>
          <div>{props.phone}</div>
          <div>{props.mobile}</div>
        </Container>
      </Infos>
      <Actions>
        <Icon icon="edit" color href="/compte/edition" />
      </Actions>
    </UserInfos>
  );
}

UserInfo.propTypes = {
  name: PropTypes.string.isRequired,
  address: PropTypes.string,
  postalCode: PropTypes.string,
  locality: PropTypes.string,
  phone: PropTypes.string,
  country: PropTypes.string,
  mobile: PropTypes.string,
};

export default UserInfo;
