/**
 *
 * Modal
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';

const ModalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

/* eslint-disable react/prefer-stateless-function */
class Modal extends React.PureComponent {
  render() {
    return (
      <ModalWrapper>
        <div>Etes-vous sûr de vouloir supprimer ce destinataire ?</div>
        <div>
          <Button margin small onClick={this.props.onAcceptClick}>
            Oui
          </Button>
          <Button margin small onClick={this.props.onDenyClick}>
            Non
          </Button>
        </div>
      </ModalWrapper>
    );
  }
}

Modal.propTypes = {
  onAcceptClick: PropTypes.func,
  onDenyClick: PropTypes.func,
};

export default Modal;
