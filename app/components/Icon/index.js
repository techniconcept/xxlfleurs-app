/**
 *
 * Icon
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import history from '../../utils/history';
import { ChooseIcon } from '../../utils/icons';

const IconElement = styled.button`
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  color: gray;
  background: transparent;
  border: 0;
  transition: all 120ms ease-in;
  &:hover {
    color: rgba(255, 112, 0, 1);
  }

  ${props =>
    props.color &&
    css`
      color: rgba(255, 112, 0, 1);
      &:hover {
        color: gray;
      }
    `};

  ${props =>
    props.red &&
    css`
      color: red;
    `};

  ${props =>
    props.left &&
    css`
      position: absolute;
      left: 15px;
      display: flex;
      padding: 0;
      margin: 0;
      div {
        flex: 1;
        width: 30px;
        height: 30px;
        margin: 0;
      }
    `};

  ${props =>
    props.right &&
    css`
      position: absolute;
      right: 15px;
      display: flex;
      padding: 0;
      margin: 0;
      div {
        flex: 1;
        width: 30px;
        height: 30px;
        margin: 0;
      }
    `};
`;

const Media = styled.div`
  width: 2.5em;
  height: 100%;
  display: flex;
  align-items: center;
  ${props =>
    props.small &&
    css`
      width: 1.5em;
    `};
`;

const Label = styled.label`
  font-family: 'Ubuntu', sans-serif;
  text-transform: uppercase;
  cursor: pointer;
  align-self: flex-end;
  display: block;
`;

// eslint-disable-next-line func-names
const Icon = function(props) {
  const params = {
    pathname: props.href || '#',
    state: { resetStep: props.resetStep },
  };
  return (
    <IconElement
      onClick={() => (props.onClick ? props.onClick() : history.push(params))}
      type="button"
      color={props.color ? 'color' : null}
      red={props.red}
      left={props.left}
      right={props.right}
    >
      <Media small={props.small}>{ChooseIcon(props.icon)}</Media>
      <Label>{props.label}</Label>
    </IconElement>
  );
};

Icon.propTypes = {
  icon: PropTypes.oneOf([
    'truck',
    'checked',
    'unchecked',
    'history',
    'plane',
    'users',
    'user',
    'edit',
    'trash',
    'add',
    'redo',
    'xxl',
    'arrow-left',
    'arrow-right',
    'subscription',
  ]),
  href: PropTypes.string,
  color: PropTypes.bool,
  red: PropTypes.bool,
  label: PropTypes.string,
  left: PropTypes.bool,
  right: PropTypes.bool,
  onClick: PropTypes.func,
  small: PropTypes.bool,
  resetStep: PropTypes.bool,
};

export default Icon;
