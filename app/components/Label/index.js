/**
 *
 * Label
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const LabelElement = styled.label`
  display: block;
  font-family: 'Ubuntu', sans-serif;
  font-size: 16px;
  line-height: 16px;
  margin-bottom: 1em;
  ${props =>
    props.error &&
    css`
      color: red;
    `};
`;

const LabelWrapper = styled.div`
  margin-bottom: 0.3em;
`;

const InputWrapper = styled.div`
  margin-bottom: 0.3em;
`;

function Label(props) {
  return (
    <LabelElement error={props.error}>
      <LabelWrapper>{props.label}</LabelWrapper>
      <InputWrapper>{props.children}</InputWrapper>
    </LabelElement>
  );
}

Label.propTypes = {
  label: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  error: PropTypes.bool,
};

export default Label;
